-- Revert gis-sc_vlci:r_code/functions/tg.t_env_medioambiente_visualizacion_estaciones_insert from pg

BEGIN;

DROP trigger IF EXISTS t_env_medioambiente_visualizacion_estaciones_before_insert on sc_vlci.t_env_medioambiente_visualizacion_estaciones;

COMMIT;
