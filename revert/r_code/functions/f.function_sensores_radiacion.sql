-- Revert gis-sc_vlci:r_code/functions/f.function_sensores_radiacion from pg

BEGIN;

CREATE OR REPLACE FUNCTION sc_vlci.function_sensores_radiacion()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE v_id int;
BEGIN
    v_id := (select right(new.entityid,position('_' in reverse(new.entityid))-1)::int);

    UPDATE gis.sensores_radiacion_solar_evw
    SET uvindex = new.uvindex::real
    WHERE id = v_id;

           RETURN new;
END;
$function$
;


COMMIT;
