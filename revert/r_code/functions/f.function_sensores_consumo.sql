-- Revert gis-sc_vlci:r_code/functions/f.function_sensores_consumo from pg

BEGIN;

CREATE OR REPLACE FUNCTION sc_vlci.function_sensores_consumo()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE v_id int;
BEGIN
    v_id := (select right(new.entityid,position('_' in reverse(new.entityid))-1)::int);

    UPDATE gis.sensores_consumo_evw
    SET potencia_activa = new.power_active::real, 
    	potencia_activa1 = new.power_active1::real,
    	potencia_activa2 = new.power_active2::real,
    	potencia_activa3 = new.power_active3::real,
    	potencia_factor = new.power_factor::real,
    	potencia_factor1 = new.power_factor1::real,
    	potencia_factor2 = new.power_factor2::real,
    	potencia_factor3 = new.power_factor3::real,
    	potencia_reactiva = new.power_reactive::real,
    	potencia_reactiva1 = new.power_reactive1::real,
    	potencia_reactiva2 = new.power_reactive2::real,
    	potencia_reactiva3 = new.power_reactive3::real,
        energia_fase3 = new.energy_3phase::real
    WHERE id = v_id;

           RETURN new;
END;
$function$
;


COMMIT;
