-- Revert gis-sc_vlci:r_code/functions/f.fn_aud_fec_upd from pg

BEGIN;

    DROP FUNCTION IF EXISTS sc_vlci.fn_aud_fec_upd() CASCADE;

COMMIT;
