-- Revert gis-sc_vlci:r_code/functions/f.current_timestamp_before_insert from pg

BEGIN;

DROP FUNCTION sc_vlci.fn_current_timestamp_before_insert();

COMMIT;
