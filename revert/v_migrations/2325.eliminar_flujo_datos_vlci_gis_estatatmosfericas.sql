-- Revert gis-sc_vlci:v_migrations/2325.eliminar_flujo_datos_vlci_gis_estatmosfericas from pg
BEGIN;

CREATE
OR REPLACE FUNCTION sc_vlci.update_estatatmosfericas_medidas () RETURNS trigger LANGUAGE plpgsql AS $function$
BEGIN
   UPDATE gis.estatatmosfericas_evw SET 
   viento_direccion=cast(NEW.windDirectionValue AS NUMERIC(4,1)),
   viento_velocidad=cast(NEW.windSpeedValue AS NUMERIC(4,1)),
   temperatura=cast(NEW.temperatureValue AS NUMERIC(4,1)),
   humedad_relativa=CASE WHEN cast(NEW.relativeHumidityValue AS NUMERIC(4,1)) <= 100 THEN cast(NEW.relativeHumidityValue AS NUMERIC(4,1)) ELSE humedad_relativa END,
   presion_bar=cast(NEW.atmosphericPressureValue AS NUMERIC(5,1)),
   precipitaciones=cast(NEW.precipitationValue AS NUMERIC(5,1)),
   fecha_carga=NEW.recvTime
   WHERE nombre=NEW.gis_id;
   RETURN NEW;
END;
$function$;

COMMIT;