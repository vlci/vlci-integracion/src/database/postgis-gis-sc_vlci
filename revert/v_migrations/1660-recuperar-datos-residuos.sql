-- Revert gis-sc_vlci:v_migrations/1660-recuperar-datos-residuos from pg

BEGIN;

delete from sc_vlci.residuos_any_keyperformanceindicator
where sliceanddicevalue2 = 'Vidre'
and TO_DATE(calculationperiod ,'DD-MM-YYYY') >= '2020-03-01'
and TO_DATE(calculationperiod ,'DD-MM-YYYY') < '2022-01-01';

-- Borrado del resto de contenedores

delete from sc_vlci.residuos_any_keyperformanceindicator
where TO_DATE(calculationperiod ,'DD-MM-YYYY') >= '2020-03-01'
and TO_DATE(calculationperiod ,'DD-MM-YYYY') < '2020-10-01';

COMMIT;
