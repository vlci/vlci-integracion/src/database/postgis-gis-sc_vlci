-- Revert gis-sc_vlci:v_migrations/3485.eliminar_flujo_datos_vlci_gis_road from pg

BEGIN;

  CREATE OR REPLACE FUNCTION sc_vlci.update_estadotrafico_realtime()
  RETURNS trigger
  LANGUAGE plpgsql
  AS $function$
  BEGIN
    UPDATE gis.tra_estado_trafico_evw SET 
    estado= cast(NEW.estadoForzado AS INTEGER)
    WHERE idtramo=cast(NEW.idTramo AS INTEGER);
    RETURN NEW;
  END;
  $function$
  ;


  CREATE TABLE sc_vlci.trafico_any_road (
    recvtime timestamp NOT NULL,
    fiwareservicepath varchar(100) NOT NULL,
    entitytype varchar(100) NOT NULL,
    entityid varchar(100) NOT NULL,
    idtramo varchar(100) DEFAULT NULL::character varying NULL,
    idtramo_md varchar(100) DEFAULT NULL::character varying NULL,
    estadoforzado varchar(100) DEFAULT NULL::character varying NULL,
    estadoforzado_md varchar(100) DEFAULT NULL::character varying NULL,
    "name" varchar(100) DEFAULT NULL::character varying NULL,
    name_md varchar(100) DEFAULT NULL::character varying NULL,
    fechahora timestamp NULL,
    fechahora_md varchar(100) DEFAULT NULL::character varying NULL,
    CONSTRAINT trafico_any_road_pkey PRIMARY KEY (recvtime, fiwareservicepath, entitytype, entityid)
  );

  -- Table Triggers
  create trigger insert_road_to_estadotrafico before
  insert
      on
      sc_vlci.trafico_any_road for each row execute procedure update_estadotrafico_realtime();

COMMIT;

