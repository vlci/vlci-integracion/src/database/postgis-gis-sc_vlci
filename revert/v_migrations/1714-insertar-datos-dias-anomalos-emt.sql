-- Revert gis-sc_vlci:v_migrations/1714-insertar-datos-dias-anomalos-emt from pg

BEGIN;

delete from sc_vlci.emt_any_keyperformanceindicator  eak
where
	sliceanddice1 in('Titulo', 'Ruta')
	and calculationperiod in ('2023/05/28', '2023/05/23', '2023/05/21', '2023/05/20', '2023/05/19'); 


COMMIT;
