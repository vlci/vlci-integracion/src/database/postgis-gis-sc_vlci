-- Revert gis-sc_vlci:v_migrations/1714-eliminar-datos-anomalos-emt from pg

BEGIN;

INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-20 20:34:05.7','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',32,'[]','Titulo','[]','Familiar Empleado EMT','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:06.121','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',2450,'[]','Titulo','[]','Abono Especial','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:06.173','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',37,'[]','Titulo','[]','Pase Empleado EMT','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:06.211','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',1254,'[]','Titulo','[]','Bonobús','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:06.252','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',198,'[]','Titulo','[]','Bono Infantil','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:06.287','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',675,'[]','Titulo','[]','Amb Tu','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:06.325','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',39,'[]','Titulo','[]','T Joven  ','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:06.626','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',2,'[]','Titulo','[]','SUMA T-2','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:06.813','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',21,'[]','Titulo','[]','Abono Violeta','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:06.884','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',3,'[]','Titulo','[]','SUMA T-3','[]','V','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-20 20:34:07.31','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',10,'[]','Titulo','[]','SUMA T-2+','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:07.139','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',17,'[]','Titulo','[]','BB Personalizado Especial','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:07.386','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',3,'[]','Titulo','[]','Estudiante FGV','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:07.419','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',406,'[]','Titulo','[]','Billete Ordinario','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:07.653','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',2,'[]','Titulo','[]','EMT Mascota','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:09.165','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',49,'[]','Titulo','[]','BB Personalizado General','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:09.209','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',2185,'[]','Titulo','[]','SUMA 10','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:09.362','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',8,'[]','Titulo','[]','SUMA Mensual Jove','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:09.408','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',3,'[]','Titulo','[]','FGV Pase Empleado','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:09.489','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',3,'[]','Titulo','[]','Pensionista EMT','[]','V','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-20 20:34:09.62','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',10,'[]','Titulo','[]','Pase Ayuntamiento','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:09.724','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',57,'[]','Titulo','[]','SUMA T-3+','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:24.508','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',6,'[]','Titulo','[]','SUMA T-1+','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:26.825','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',821,'[]','Titulo','[]','SUMA Mensual','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:27.383','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',1657,'[]','Titulo','[]','BonoOro','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:27.582','/emt','KeyPerformanceIndicator','mob006-01','2023-05-19','[]',6,'[]','Titulo','[]','Familiar Pensionista EMT','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-20 20:34:27.7','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',283,'[]','Ruta','[]','12','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:27.754','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',229,'[]','Ruta','[]','18','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:28.421','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',351,'[]','Ruta','[]','19','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:28.671','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',29,'[]','Ruta','[]','1','[]','V','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-20 20:34:28.733','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',677,'[]','Ruta','[]','8','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:28.994','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',380,'[]','Ruta','[]','9','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:29.32','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',306,'[]','Ruta','[]','60','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:29.264','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',200,'[]','Ruta','[]','62','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:30.35','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',158,'[]','Ruta','[]','40','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:30.15','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',120,'[]','Ruta','[]','63','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:30.185','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',168,'[]','Ruta','[]','64','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:30.415','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',20,'[]','Ruta','[]','67','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:30.463','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',281,'[]','Ruta','[]','25','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:30.884','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',326,'[]','Ruta','[]','27','[]','V','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-20 20:34:30.969','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',489,'[]','Ruta','[]','28','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:31.32','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',22,'[]','Ruta','[]','98E','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:31.65','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',421,'[]','Ruta','[]','C2','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:31.118','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',576,'[]','Ruta','[]','C3','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:31.156','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',751,'[]','Ruta','[]','70','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:31.428','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',837,'[]','Ruta','[]','93','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:31.583','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',240,'[]','Ruta','[]','94','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:31.644','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',640,'[]','Ruta','[]','95','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:31.71','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',350,'[]','Ruta','[]','30','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-20 20:34:31.747','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',478,'[]','Ruta','[]','98','[]','V','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-20 20:34:31.796','/emt','KeyPerformanceIndicator','mob006-04','2023-05-19','[]',1622,'[]','Ruta','[]','99','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-21 20:34:04.975','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',98,'[]','Titulo','[]','Abono Especial','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:05.38','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',1,'[]','Titulo','[]','Familiar Empleado EMT','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:05.777','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',1,'[]','Titulo','[]','BB Personalizado General','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:06.69','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',61,'[]','Titulo','[]','SUMA 10','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:07.289','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',29,'[]','Titulo','[]','Bonobús','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:07.354','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',2,'[]','Titulo','[]','Bono Infantil','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:07.479','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',19,'[]','Titulo','[]','Amb Tu','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:07.726','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',9,'[]','Titulo','[]','SUMA T-3+','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:07.965','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',1,'[]','Titulo','[]','T Joven  ','[]','S','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-21 20:34:08.171','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',2,'[]','Titulo','[]','Abono Violeta','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:08.431','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',35,'[]','Titulo','[]','SUMA Mensual','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:08.494','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',12,'[]','Titulo','[]','BonoOro','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:08.556','/emt','KeyPerformanceIndicator','mob006-01','2023-05-20','[]',19,'[]','Titulo','[]','Billete Ordinario','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-21 20:34:08.595','/emt','KeyPerformanceIndicator','mob006-04','2023-05-20','[]',2,'[]','Ruta','[]','1','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-21 20:34:08.651','/emt','KeyPerformanceIndicator','mob006-04','2023-05-20','[]',7,'[]','Ruta','[]','25','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-21 20:34:08.71','/emt','KeyPerformanceIndicator','mob006-04','2023-05-20','[]',90,'[]','Ruta','[]','19','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-21 20:34:08.748','/emt','KeyPerformanceIndicator','mob006-04','2023-05-20','[]',27,'[]','Ruta','[]','9','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-21 20:34:09.1','/emt','KeyPerformanceIndicator','mob006-04','2023-05-20','[]',163,'[]','Ruta','[]','C3','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-22 20:34:05.475','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21','[]',44,'[]','Titulo','[]','Abono Especial','[]','D','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-22 20:34:05.581','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21','[]',1,'[]','Titulo','[]','SUMA T-1','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-22 20:34:05.649','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21','[]',6,'[]','Titulo','[]','Amb Tu','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-22 20:34:05.92','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21','[]',1,'[]','Titulo','[]','Abono Violeta','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-22 20:34:05.98','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21','[]',24,'[]','Titulo','[]','SUMA 10','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-22 20:34:07.522','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21','[]',1,'[]','Titulo','[]','FGV Pase Empleado','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-22 20:34:08.641','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21','[]',3,'[]','Titulo','[]','SUMA Mensual','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-22 20:34:08.712','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21','[]',7,'[]','Titulo','[]','Bonobús','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-22 20:34:08.769','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21','[]',1,'[]','Titulo','[]','BonoOro','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-22 20:34:08.81','/emt','KeyPerformanceIndicator','mob006-01','2023-05-21','[]',39,'[]','Titulo','[]','Billete Ordinario','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-22 20:34:08.859','/emt','KeyPerformanceIndicator','mob006-04','2023-05-21','[]',2,'[]','Ruta','[]','1','[]','D','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-22 20:34:08.914','/emt','KeyPerformanceIndicator','mob006-04','2023-05-21','[]',80,'[]','Ruta','[]','19','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-22 20:34:08.981','/emt','KeyPerformanceIndicator','mob006-04','2023-05-21','[]',16,'[]','Ruta','[]','73','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-22 20:34:09.22','/emt','KeyPerformanceIndicator','mob006-04','2023-05-21','[]',29,'[]','Ruta','[]','C3','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:05.44','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',13421,'[]','Titulo','[]','Abono Especial','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:06.579','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',156,'[]','Titulo','[]','Familiar Empleado EMT','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:06.804','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',161,'[]','Titulo','[]','Pase Empleado EMT','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.2','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',1,'[]','Titulo','[]','Valencia Card 1 día','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.72','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',7464,'[]','Titulo','[]','Bonobús','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.107','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',163,'[]','Titulo','[]','EMV','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.256','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',1281,'[]','Titulo','[]','EMTicket','[]','M','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-24 20:34:08.324','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',1028,'[]','Titulo','[]','Bono Infantil','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.379','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',15,'[]','Titulo','[]','SUMA T-1','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.412','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',3489,'[]','Titulo','[]','Amb Tu','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.444','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',182,'[]','Titulo','[]','T Joven  ','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.491','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',18,'[]','Titulo','[]','SUMA T-2','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.532','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',42,'[]','Titulo','[]','SUMA T-3','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.636','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',148,'[]','Titulo','[]','Abono Violeta','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.669','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',45,'[]','Titulo','[]','SUMA T-2+','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.702','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',102,'[]','Titulo','[]','BB Personalizado Especial','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:08.753','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',26,'[]','Titulo','[]','Estudiante FGV','[]','M','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-24 20:34:09.24','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',2317,'[]','Titulo','[]','Billete Ordinario','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:09.79','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',3,'[]','Titulo','[]','EMT Mascota','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:09.164','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',237,'[]','Titulo','[]','BB Personalizado General','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:09.25','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',12466,'[]','Titulo','[]','SUMA 10','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:09.318','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',48,'[]','Titulo','[]','SUMA Mensual Jove','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:09.412','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',1,'[]','Titulo','[]','Pase Evento','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:09.476','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',35,'[]','Titulo','[]','FGV Pase Empleado','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:09.545','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',21,'[]','Titulo','[]','Pensionista EMT','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:10.5','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',83,'[]','Titulo','[]','Pase Ayuntamiento','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:10.177','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',311,'[]','Titulo','[]','SUMA T-3+','[]','M','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-24 20:34:10.255','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',22,'[]','Titulo','[]','SUMA T-1+','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:10.287','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',1,'[]','Titulo','[]','Pase Ministerio Interior','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:10.639','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',4528,'[]','Titulo','[]','SUMA Mensual','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:10.921','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',19,'[]','Titulo','[]','Familiar Pensionista EMT','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:10.955','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',11449,'[]','Titulo','[]','BonoOro','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:10.983','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',4,'[]','Titulo','[]','Jubilado FGV','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:11.8','/emt','KeyPerformanceIndicator','mob006-01','2023-05-23','[]',3,'[]','Titulo','[]','Pase Estudiante EMT','[]','M','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-24 20:34:11.36','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',938,'[]','Ruta','[]','12','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:11.62','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',419,'[]','Ruta','[]','35','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:11.29','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',1546,'[]','Ruta','[]','18','[]','M','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-24 20:34:11.372','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',262,'[]','Ruta','[]','19','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:11.414','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',70,'[]','Ruta','[]','1','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:11.464','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',691,'[]','Ruta','[]','4','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:11.565','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',4007,'[]','Ruta','[]','6','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:11.609','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',2112,'[]','Ruta','[]','7','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:11.639','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',881,'[]','Ruta','[]','8','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:11.739','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',717,'[]','Ruta','[]','81','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:11.818','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',959,'[]','Ruta','[]','9','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:12.56','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',823,'[]','Ruta','[]','60','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:12.82','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',388,'[]','Ruta','[]','40','[]','M','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-24 20:34:12.175','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',240,'[]','Ruta','[]','62','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:12.442','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',104,'[]','Ruta','[]','63','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:12.61','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',1138,'[]','Ruta','[]','64','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:12.941','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',464,'[]','Ruta','[]','67','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:13.64','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',241,'[]','Ruta','[]','25','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:13.99','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',3479,'[]','Ruta','[]','27','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:13.147','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',399,'[]','Ruta','[]','28','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:13.236','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',19,'[]','Ruta','[]','98E','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:13.354','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',2870,'[]','Ruta','[]','C1','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:13.425','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',1954,'[]','Ruta','[]','C3','[]','M','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-24 20:34:13.559','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',1559,'[]','Ruta','[]','92','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:14.198','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',1909,'[]','Ruta','[]','70','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:14.348','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',5537,'[]','Ruta','[]','93','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:14.61','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',3530,'[]','Ruta','[]','71','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:14.787','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',207,'[]','Ruta','[]','94','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:14.899','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',3582,'[]','Ruta','[]','95','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:15.69','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',3004,'[]','Ruta','[]','73','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:15.441','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',342,'[]','Ruta','[]','30','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:15.71','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',13,'[]','Ruta','[]','96','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:15.85','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',244,'[]','Ruta','[]','31','[]','M','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-24 20:34:15.892','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',1537,'[]','Ruta','[]','10','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:15.919','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',681,'[]','Ruta','[]','98','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:15.965','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',357,'[]','Ruta','[]','32','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:16.24','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',10500,'[]','Ruta','[]','99','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-24 20:34:16.109','/emt','KeyPerformanceIndicator','mob006-04','2023-05-23','[]',1567,'[]','Ruta','[]','11','[]','M','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:06.309','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',27849,'[]','Titulo','[]','Abono Especial','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:07.853','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',195,'[]','Titulo','[]','Familiar Empleado EMT','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:08.12','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',303,'[]','Titulo','[]','Pase Empleado EMT','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:08.163','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',12008,'[]','Titulo','[]','Bonobús','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:08.206','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',138,'[]','Titulo','[]','EMV','[]','D','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-29 20:34:08.457','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',1707,'[]','Titulo','[]','Bono Infantil','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:08.496','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',123,'[]','Titulo','[]','SUMA T-1','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:08.532','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',131,'[]','Titulo','[]','SUMA T-2','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:08.631','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',6194,'[]','Titulo','[]','Amb Tu','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:08.896','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',324,'[]','Titulo','[]','T Joven  ','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:09.167','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',202,'[]','Titulo','[]','Abono Violeta','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:09.21','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',235,'[]','Titulo','[]','SUMA T-3','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:10.66','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',454,'[]','Titulo','[]','SUMA T-2+','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:10.132','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',120,'[]','Titulo','[]','BB Personalizado Especial','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:10.429','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',18,'[]','Titulo','[]','Estudiante FGV','[]','D','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-29 20:34:10.474','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',11257,'[]','Titulo','[]','Billete Ordinario','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:10.535','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',22,'[]','Titulo','[]','EMT Mascota','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:10.603','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',198,'[]','Titulo','[]','BB Personalizado General','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:10.848','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',27013,'[]','Titulo','[]','SUMA 10','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:10.907','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',57,'[]','Titulo','[]','SUMA Mensual Jove','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:11.217','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',5,'[]','Titulo','[]','Pase Evento','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:11.298','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',50,'[]','Titulo','[]','FGV Pase Empleado','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:11.574','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',37,'[]','Titulo','[]','Pensionista EMT','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:11.618','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',10,'[]','Titulo','[]','Pase Ayuntamiento','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:11.654','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',2023,'[]','Titulo','[]','SUMA T-3+','[]','D','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-29 20:34:11.709','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',125,'[]','Titulo','[]','SUMA T-1+','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:12.6','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',4,'[]','Titulo','[]','Pase Ministerio Interior','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:12.255','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',7346,'[]','Titulo','[]','SUMA Mensual','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:12.297','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',22735,'[]','Titulo','[]','BonoOro','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:12.364','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',49,'[]','Titulo','[]','Familiar Pensionista EMT','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:12.399','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',7,'[]','Titulo','[]','Jubilado FGV','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:12.502','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',2,'[]','Titulo','[]','Pase Estudiante EMT','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:12.57','/emt','KeyPerformanceIndicator','mob006-01','2023-05-28','[]',7,'[]','Titulo','[]','Valencia Card 3 días','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-05-29 20:34:12.847','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1552,'[]','Ruta','[]','12','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:13.116','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',2671,'[]','Ruta','[]','35','[]','D','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-29 20:34:13.18','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',474,'[]','Ruta','[]','13','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:13.223','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1523,'[]','Ruta','[]','14','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:13.27','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',2644,'[]','Ruta','[]','16','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:13.32','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',814,'[]','Ruta','[]','18','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:13.368','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',5041,'[]','Ruta','[]','19','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:13.443','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',19,'[]','Ruta','[]','1','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:13.496','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',4189,'[]','Ruta','[]','4','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:13.954','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',3343,'[]','Ruta','[]','6','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:14.303','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1853,'[]','Ruta','[]','7','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:14.579','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1773,'[]','Ruta','[]','8','[]','D','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-29 20:34:14.637','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',3638,'[]','Ruta','[]','9','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:14.698','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',2789,'[]','Ruta','[]','81','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:15.282','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',3154,'[]','Ruta','[]','60','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:15.352','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',2279,'[]','Ruta','[]','62','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:15.588','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1091,'[]','Ruta','[]','40','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:15.621','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',3009,'[]','Ruta','[]','64','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:15.651','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1007,'[]','Ruta','[]','67','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:16.153','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',250,'[]','Ruta','[]','23','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:16.189','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',820,'[]','Ruta','[]','24','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:16.254','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',3317,'[]','Ruta','[]','25','[]','D','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-29 20:34:16.324','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',720,'[]','Ruta','[]','26','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:16.382','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',3836,'[]','Ruta','[]','27','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:16.441','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',149,'[]','Ruta','[]','E','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:16.506','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',2622,'[]','Ruta','[]','28','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:16.541','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',12,'[]','Ruta','[]','98E','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:16.573','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1654,'[]','Ruta','[]','C1','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:16.826','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1695,'[]','Ruta','[]','C2','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:16.886','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',13945,'[]','Ruta','[]','C3','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:17.162','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',2870,'[]','Ruta','[]','70','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:17.412','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',4191,'[]','Ruta','[]','92','[]','D','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-29 20:34:17.642','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',3950,'[]','Ruta','[]','93','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:17.695','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',2762,'[]','Ruta','[]','71','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:17.937','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1299,'[]','Ruta','[]','72','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:17.99','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1762,'[]','Ruta','[]','94','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:18.24','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1649,'[]','Ruta','[]','73','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:18.54','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',6061,'[]','Ruta','[]','95','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:18.287','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',780,'[]','Ruta','[]','30','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:18.332','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',11,'[]','Ruta','[]','96','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:18.365','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',2525,'[]','Ruta','[]','31','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:18.41','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',1786,'[]','Ruta','[]','98','[]','D','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-05-29 20:34:18.475','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',2988,'[]','Ruta','[]','10','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:18.559','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',4426,'[]','Ruta','[]','32','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:18.684','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',2982,'[]','Ruta','[]','11','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-05-29 20:34:18.729','/emt','KeyPerformanceIndicator','mob006-04','2023-05-28','[]',9023,'[]','Ruta','[]','99','[]','D','[]',NULL,NULL,NULL,NULL);

COMMIT;
