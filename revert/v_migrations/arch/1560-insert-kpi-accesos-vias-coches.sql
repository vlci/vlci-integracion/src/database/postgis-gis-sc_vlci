-- Revert gis-sc_vlci:v_migrations/1560-insert-kpi-accesos-vias-coches from pg

BEGIN;

delete from sc_vlci.trafico_any_keyperformanceindicator where entityid = 'Kpi-Accesos-Vias-Coches' and calculationperiod < '2020-11-28';

COMMIT;
