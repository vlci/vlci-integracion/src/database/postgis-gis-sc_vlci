-- Revert gis-sc_vlci:v_migrations/1562-insert-kpi-trafico-bicicletas-anillo from pg

BEGIN;

delete from sc_vlci.trafico_any_keyperformanceindicator where entityid = 'Kpi-Trafico-Bicicletas-Anillo' and calculationperiod < '2020-11-28';

COMMIT;
