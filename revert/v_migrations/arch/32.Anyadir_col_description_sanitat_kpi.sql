-- Revert gis-sc_vlci:v_migrations/32.Anyadir_col_description_sanitat_kpi from pg

BEGIN;

ALTER TABLE sc_vlci.sanitat_any_keyperformanceindicator DROP COLUMN description;
ALTER TABLE sc_vlci.sanitat_any_keyperformanceindicator DROP COLUMN description_md;

COMMIT;
