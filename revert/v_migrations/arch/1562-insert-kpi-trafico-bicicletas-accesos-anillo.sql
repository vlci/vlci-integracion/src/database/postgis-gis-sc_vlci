-- Revert gis-sc_vlci:v_migrations/1562-insert-Kpi-trafico-bicicletas-accesos-anillo from pg

BEGIN;

delete from sc_vlci.trafico_any_keyperformanceindicator where entityid = 'Kpi-Trafico-Bicicletas-Accesos-Anillo' and calculationperiod < '2020-11-28';

COMMIT;
