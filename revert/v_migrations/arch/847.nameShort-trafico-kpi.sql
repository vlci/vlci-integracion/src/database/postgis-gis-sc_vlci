-- Revert gis-sc_vlci:v_migrations/847.nameShort-trafico-kpi from pg

BEGIN;

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Accés Barcelona [entrada i eixida]';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Prolongació Joan XXIII';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Av. del Cid';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Accés per V-31 [Pista de Silla]';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Corts Valencianes [Accés per CV-35]';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Accés a Arxiduc Carles pel Camí nou de Picanya';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Gran Via de Ferran el Catòlic';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Av. Blasco Ibáñez';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Av. de Peris i Valero';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Gran Via del Marqués del Túria';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Av. de Giorgeta';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort= null
WHERE name='Av. Dr. Peset Aleixandre';

ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator DROP COLUMN nameshort;
ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator DROP COLUMN nameshort_md;

COMMIT;
