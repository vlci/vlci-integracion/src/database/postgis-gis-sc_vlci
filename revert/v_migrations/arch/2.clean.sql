-- Revert gis-sc_vlci:v_migrations/2.clean from pg

BEGIN;

CREATE TABLE IF NOT EXISTS sc_vlci.test_table (
    recvtime timestamp without time zone NOT NULL,
    fiwareservicepath character varying(100) NOT NULL,
    entitytype character varying(100) NOT NULL,
    entityid character varying(100) NOT NULL,
    calculationperiod date,
    calculationperiod_md character varying(100) DEFAULT NULL::character varying,
    kpivalue numeric,
    kpivalue_md character varying(100) DEFAULT NULL::character varying,
    sliceanddice1 character varying(100) DEFAULT NULL::character varying,
    sliceanddice1_md character varying(100) DEFAULT NULL::character varying,
    sliceanddicevalue1 character varying(100) DEFAULT NULL::character varying,
    sliceanddicevalue1_md character varying(100) DEFAULT NULL::character varying,
    diasemana character varying(100) DEFAULT NULL::character varying,
    diasemana_md character varying(100) DEFAULT NULL::character varying
);

COMMIT;
