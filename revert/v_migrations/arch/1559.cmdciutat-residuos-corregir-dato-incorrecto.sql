-- Revert gis-sc_vlci:v_migrations/1559.cmdciutat-residuos-corregir-dato-incorrecto from pg

BEGIN;

update sc_vlci.residuos_any_keyperformanceindicator set kpivalue = '358490'  
where calculationperiod = '04-01-2023' and sliceanddicevalue1  = 'Zona 2' and sliceanddicevalue2 = 'Orgànica';

update sc_vlci.residuos_any_keyperformanceindicator set kpivalue = '427410'  
where calculationperiod = '04-01-2023' and sliceanddicevalue1  = 'Total' and sliceanddicevalue2 = 'Orgànica';

COMMIT;
