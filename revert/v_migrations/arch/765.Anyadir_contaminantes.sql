-- Revert gis-sc_vlci:v_migrations/765.Anyadir_contaminantes from pg

BEGIN;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN novalue;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN novalue_md;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN noxvalue;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN noxvalue_md;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN pm1value;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN pm1value_md;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN notype;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN notype_md;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN noxtype;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN noxtype_md;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN pm1type;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN pm1type_md;

COMMIT;
