-- Revert gis-sc_vlci:r_env_tables/tb.t_env_medioambiente_visualizacion_estaciones from pg

BEGIN;

DELETE FROM sc_vlci.t_env_medioambiente_visualizacion_estaciones
WHERE entityid IN (
    'A01_AVFRANCIA',
    'A02_BULEVARDSUD',
    'A03_MOLISOL',
    'A04_PISTASILLA',
    'A05_POLITECNIC',
    'A06_VIVERS',
    'A07_VALENCIACENTRE',
    'A08_DR_LLUCH',
    'A09_CABANYAL',
    'A10_OLIVERETA',
    'A11_PATRAIX');

COMMIT;
