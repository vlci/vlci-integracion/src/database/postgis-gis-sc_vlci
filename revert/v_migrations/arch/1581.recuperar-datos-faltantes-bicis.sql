-- Revert gis-sc_vlci:v_migrations/1581.recuperar-datos-faltantes-bicis from pg

BEGIN;

INSERT INTO trafico_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,diasemana,diasemana_md,kpivalue,kpivalue_md,tramo,tramo_md,"name",name_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md,nameshort,nameshort_md) VALUES
	 ('2021-02-02 01:00:14.823','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Accesos-Anillo','2021-02-01','[]','L','[]',1881,'[]','Pont de fusta','[]','Pont de fusta','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-02-02 01:00:15.991','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Accesos-Anillo','2021-02-01','[]','L','[]',1211,'[]','Pont del real','[]','Pont del real','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-02-02 01:00:17.76','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Accesos-Anillo','2021-02-01','[]','L','[]',2240,'[]','Carrer Russafa','[]','Carrer Russafa','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-07-06 00:00:16.331','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Accesos-Anillo','2021-07-05','[]','L','[]',3053,'[]','Navarro Reverter','[]','Navarro Reverter','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-07-06 00:00:16.758','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Accesos-Anillo','2021-07-05','[]','L','[]',2858,'[]','Carrer Alacant','[]','Carrer Alacant','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-07-06 00:00:16.832','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Accesos-Anillo','2021-07-05','[]','L','[]',3079,'[]','Carrer Russafa','[]','Carrer Russafa','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-07-06 00:00:16.446','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Accesos-Anillo','2021-07-05','[]','L','[]',2627,'[]','Pont de fusta','[]','Pont de fusta','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-07-06 00:00:16.582','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Accesos-Anillo','2021-07-05','[]','L','[]',3172,'[]','Pont de les arts','[]','Pont de les arts','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-07-06 00:00:16.666','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Accesos-Anillo','2021-07-05','[]','L','[]',1612,'[]','Pont del real','[]','Pont del real','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL);

INSERT INTO trafico_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,diasemana,diasemana_md,kpivalue,kpivalue_md,tramo,tramo_md,"name",name_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md,nameshort,nameshort_md) VALUES
	 ('2021-02-02 01:00:07.249','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Anillo','2021-02-01','[]','L','[]',3281,'[]','Carrer Colon','[]','Carrer Colon','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-02-02 01:00:12.465','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Anillo','2021-02-01','[]','L','[]',3393,'[]','Guillem de Castro','[]','Guillem de Castro','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-02-02 01:00:13.415','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Anillo','2021-02-01','[]','L','[]',1354,'[]','Plaça Tetuan','[]','Plaça Tetuan','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-07-06 00:00:13.426','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Anillo','2021-07-05','[]','L','[]',2674,'[]','Comte de Trénor – Pont de fusta','[]','Comte de Trénor – Pont de fusta','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-07-06 00:00:15.227','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Anillo','2021-07-05','[]','L','[]',4245,'[]','Guillem de Castro','[]','Guillem de Castro','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-07-06 00:00:16.211','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Anillo','2021-07-05','[]','L','[]',2008,'[]','Plaça Tetuan','[]','Plaça Tetuan','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-07-06 00:00:12.974','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Anillo','2021-07-05','[]','L','[]',4574,'[]','Carrer Colon','[]','Carrer Colon','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL),
	 ('2021-07-06 00:00:16.104','/trafico','KeyPerformanceIndicator','Kpi-Trafico-Bicicletas-Anillo','2021-07-05','[]','L','[]',6009,'[]','Xàtiva','[]','Xàtiva','[]','Bicicletas',NULL,'Bicicletes',NULL,NULL,NULL);

delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-19') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-21') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-21') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-22') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-23') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-24') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-24') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-25') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-27') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-30') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-01') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-03') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-10') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-11') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-14') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-19') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-20') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-21') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-22') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-23') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-30') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-06-22') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-06-23') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-06-24') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2021-09-03') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-15') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-15') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-16') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-16') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-17') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-17') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-01') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-01') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-01') and k.tramo = 'Pont del real'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-04') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-05') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-06') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-07') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-08') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-09') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-11-03') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-11-04') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-23', '2021-08-24', '2021-08-25', '2021-08-26') and k.tramo = 'Carrer Colon'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-01') and k.tramo = 'Xàtiva'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

COMMIT;
