-- Revert gis-sc_vlci:v_migrations/342.Eliminar_col_description_sanitat_kpi from pg

BEGIN;

ALTER TABLE sc_vlci.sanitat_any_keyperformanceindicator ADD description varchar NULL;
ALTER TABLE sc_vlci.sanitat_any_keyperformanceindicator ADD description_md varchar NULL;

COMMIT;
