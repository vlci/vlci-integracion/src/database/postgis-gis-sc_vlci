-- Revert gis-sc_vlci:v_migrations/1567.historicos_EMT_mob006-01_2015 from pg

BEGIN;

DELETE FROM sc_vlci.emt_any_keyperformanceindicator WHERE entityid = 'mob006-01' 
and calculationperiod < '2016-01-01'
and calculationperiod >= '2015-01-01';

COMMIT;
