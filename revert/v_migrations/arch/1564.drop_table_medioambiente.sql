-- Revert gis-sc_vlci:v_migrations/1564.drop_table_medioambiente from pg

BEGIN;

CREATE TABLE sc_vlci.medioambiente_any_keyperformanceindicator (
	recvtime timestamptz NULL,
	fiwareservicepath text NULL,
	entityid varchar(64) NOT NULL,
	entitytype text NULL,
	calculationperiod text NULL,
	kpivalue float8 NULL,
	calculationperiod_md text NULL,
	kpivalue_md text NULL,
	measurelandunit varchar(100) NULL,
	measurelandunit_md varchar(100) NULL
);

COMMIT;
