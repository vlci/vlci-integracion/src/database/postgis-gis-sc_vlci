-- Revert gis-sc_vlci:v_migrations/46.Anyadir_columnas_unidad_medida from pg

BEGIN;

ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator DROP COLUMN measureunitcas;
ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator DROP COLUMN measureunitcas_md;
ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator DROP COLUMN measureunitval;
ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator DROP COLUMN measureunitval_md ;

ALTER TABLE sc_vlci.wifi_any_keyperformanceindicator DROP COLUMN measureunitcas;
ALTER TABLE sc_vlci.wifi_any_keyperformanceindicator DROP COLUMN measureunitcas_md;
ALTER TABLE sc_vlci.wifi_any_keyperformanceindicator DROP COLUMN measureunitval;
ALTER TABLE sc_vlci.wifi_any_keyperformanceindicator DROP COLUMN measureunitval_md;

ALTER TABLE sc_vlci.emt_any_keyperformanceindicator DROP COLUMN measureunitcas;
ALTER TABLE sc_vlci.emt_any_keyperformanceindicator DROP COLUMN measureunitcas_md;
ALTER TABLE sc_vlci.emt_any_keyperformanceindicator DROP COLUMN measureunitval;
ALTER TABLE sc_vlci.emt_any_keyperformanceindicator DROP COLUMN measureunitval_md;

ALTER TABLE sc_vlci.medioambiente_any_keyperformanceindicator DROP COLUMN measurelandunit;
ALTER TABLE sc_vlci.medioambiente_any_keyperformanceindicator DROP COLUMN measurelandunit_md;

ALTER TABLE sc_vlci.aguas_any_keyperformanceindicator DROP COLUMN measurelandunit;
ALTER TABLE sc_vlci.aguas_any_keyperformanceindicator DROP COLUMN measurelandunit_md;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN no2type;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN o3type;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN pm10type;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN pm25type;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN so2type;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN no2type_md;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN o3type_md;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN pm10type_md;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN pm25type_md;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved DROP COLUMN so2type_md;

ALTER TABLE sc_vlci.residuos_any_keyperformanceindicator DROP COLUMN measureunit;
ALTER TABLE sc_vlci.residuos_any_keyperformanceindicator DROP COLUMN measureunit_md;


COMMIT;
