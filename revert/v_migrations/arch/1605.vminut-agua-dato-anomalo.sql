-- Revert gis-sc_vlci:v_migrations/1605.vminut-agua-dato-anomalo from pg

BEGIN;

INSERT INTO sc_vlci.aguas_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,"name",name_md,updatedat,updatedat_md,measurelandunit,measurelandunit_md) VALUES
	 ('2023-03-27 08:17:53.579','/aguas','KeyPerformanceIndicator','kpi-valencia-ciudad-total','2023-03-26','[]','9140.2667','[]','Consumo_agua_total_valencia','[]','2023-03-27T10:17:51.040Z','[]','m3','[]');

COMMIT;
