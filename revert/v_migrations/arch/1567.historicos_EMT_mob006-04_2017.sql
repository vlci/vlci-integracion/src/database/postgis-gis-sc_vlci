-- Revert gis-sc_vlci:v_migrations/1567.historicos_EMT_mob006-04_2017 from pg

BEGIN;

DELETE FROM sc_vlci.emt_any_keyperformanceindicator WHERE entityid = 'mob006-04' 
and calculationperiod < '2018-01-01'
and calculationperiod >= '2017-01-01';

COMMIT;
