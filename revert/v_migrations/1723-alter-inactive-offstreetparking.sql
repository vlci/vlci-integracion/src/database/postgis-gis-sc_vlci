-- Revert gis-sc_vlci:v_migrations/1723-alter-inactive-offstreetparking from pg

BEGIN;

ALTER TABLE trafico_any_offstreetparking RENAME COLUMN inactive TO operationalstatusnodataIgnore;
ALTER TABLE trafico_any_offstreetparking RENAME COLUMN inactive_md TO operationalstatusnodataIgnore_md;

COMMIT;
