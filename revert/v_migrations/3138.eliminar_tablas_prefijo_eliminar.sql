-- Revert gis-sc_vlci:v_migrations/3138.eliminar_tablas_prefijo_eliminar from pg

BEGIN;

--REVERT TABLA sc_vlci.eliminar_medioambiente_any_airqualityobserved
CREATE TABLE sc_vlci.eliminar_medioambiente_any_airqualityobserved (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(100) NOT NULL,
	entitytype varchar(100) NOT NULL,
	entityid varchar(100) NOT NULL,
	dateobserved timestamp NULL,
	dateobserved_md varchar(100) DEFAULT NULL::character varying NULL,
	so2value varchar(100) DEFAULT NULL::character varying NULL,
	so2value_md varchar(100) DEFAULT NULL::character varying NULL,
	no2value varchar(100) DEFAULT NULL::character varying NULL,
	no2value_md varchar(100) DEFAULT NULL::character varying NULL,
	o3value varchar(100) DEFAULT NULL::character varying NULL,
	o3value_md varchar(100) DEFAULT NULL::character varying NULL,
	pm10value varchar(100) DEFAULT NULL::character varying NULL,
	pm10value_md varchar(100) DEFAULT NULL::character varying NULL,
	pm25value varchar(100) DEFAULT NULL::character varying NULL,
	pm25value_md varchar(100) DEFAULT NULL::character varying NULL,
	calidad_ambiental varchar(100) DEFAULT NULL::character varying NULL,
	calidad_ambiental_md varchar(100) DEFAULT NULL::character varying NULL,
	gis_id varchar(100) DEFAULT NULL::character varying NULL,
	gis_id_md varchar(100) DEFAULT NULL::character varying NULL,
	address varchar NULL,
	address_md varchar NULL,
	no2type varchar(100) NULL,
	o3type varchar(100) NULL,
	pm10type varchar(100) NULL,
	pm25type varchar(100) NULL,
	so2type varchar(100) NULL,
	no2type_md varchar(100) NULL,
	o3type_md varchar(100) NULL,
	pm10type_md varchar(100) NULL,
	pm25type_md varchar(100) NULL,
	so2type_md varchar(100) NULL,
	novalue varchar(100) NULL,
	novalue_md varchar(100) NULL,
	noxvalue varchar(100) NULL,
	noxvalue_md varchar(100) NULL,
	pm1value varchar(100) NULL,
	pm1value_md varchar(100) NULL,
	notype varchar(100) NULL,
	notype_md varchar(100) NULL,
	noxtype varchar(100) NULL,
	noxtype_md varchar(100) NULL,
	pm1type varchar(100) NULL,
	pm1type_md varchar(100) NULL,
	CONSTRAINT medioambiente_any_airqualityobserved_pkey PRIMARY KEY (recvtime, fiwareservicepath, entitytype, entityid)
);

create trigger insert_airqualityobserved_to_estatautomaticas before
insert
    on
    sc_vlci.eliminar_medioambiente_any_airqualityobserved for each row execute procedure update_estatautomaticas_contaminantes();


--REVERT TABLA sc_vlci.eliminar_medioambiente_emt_any_keyperformanceindicator
CREATE TABLE sc_vlci.eliminar_medioambiente_emt_any_keyperformanceindicator (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(100) NOT NULL,
	entitytype varchar(100) NOT NULL,
	entityid varchar(100) NOT NULL,
	calculationperiod date NULL,
	calculationperiod_md varchar(100) DEFAULT NULL::character varying NULL,
	kpivalue numeric NULL,
	kpivalue_md varchar(100) DEFAULT NULL::character varying NULL,
	sliceanddice1 varchar(100) DEFAULT NULL::character varying NULL,
	sliceanddice1_md varchar(100) DEFAULT NULL::character varying NULL,
	sliceanddicevalue1 varchar(100) DEFAULT NULL::character varying NULL,
	sliceanddicevalue1_md varchar(100) DEFAULT NULL::character varying NULL,
	diasemana varchar(100) DEFAULT NULL::character varying NULL,
	diasemana_md varchar(100) DEFAULT NULL::character varying NULL,
	CONSTRAINT medioambiente_emt_any_keyperformanceindicator_pkey PRIMARY KEY (recvtime, fiwareservicepath, entitytype, entityid)
);
CREATE INDEX idx_emt_id_calculationperiod ON sc_vlci.eliminar_medioambiente_emt_any_keyperformanceindicator USING btree (entityid, calculationperiod);


--REVERT TABLA sc_vlci.eliminar_sanitat_any_keyperformanceindicator
CREATE TABLE sc_vlci.eliminar_sanitat_any_keyperformanceindicator (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(100) NOT NULL,
	entitytype varchar(100) NOT NULL,
	entityid varchar(100) NOT NULL,
	calculationperiod date NULL,
	calculationperiod_md varchar(100) DEFAULT NULL::character varying NULL,
	kpivalue numeric NULL,
	kpivalue_md varchar(100) DEFAULT NULL::character varying NULL,
	sliceanddicevalue1 varchar(100) DEFAULT NULL::character varying NULL,
	sliceanddicevalue1_md varchar(100) DEFAULT NULL::character varying NULL,
	inserttimestamp timestamp NULL,
	CONSTRAINT sanitat_any_keyperformanceindicator_pkey PRIMARY KEY (recvtime, fiwareservicepath, entitytype, entityid)
);
CREATE INDEX idx_sanitat_id_calperiod ON sc_vlci.eliminar_sanitat_any_keyperformanceindicator USING btree (entityid, calculationperiod);

create trigger update_sanitat_inserttimestamp before
insert
    on
    sc_vlci.eliminar_sanitat_any_keyperformanceindicator for each row execute procedure update_sanitat_inserttimestamp();


--REVERT TABLA sc_vlci.eliminar_trafico_any_offstreetparking
CREATE TABLE sc_vlci.eliminar_trafico_any_offstreetparking (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(100) NOT NULL,
	entitytype varchar(100) NOT NULL,
	entityid varchar(100) NOT NULL,
	idaparcamiento varchar(100) DEFAULT NULL::character varying NULL,
	idaparcamiento_md varchar(100) DEFAULT NULL::character varying NULL,
	"name" varchar(100) DEFAULT NULL::character varying NULL,
	name_md varchar(100) DEFAULT NULL::character varying NULL,
	address varchar(100) DEFAULT NULL::character varying NULL,
	address_md varchar(100) DEFAULT NULL::character varying NULL,
	"location" varchar(100) DEFAULT NULL::character varying NULL,
	location_md varchar(100) DEFAULT NULL::character varying NULL,
	availablespotnumber varchar(100) DEFAULT NULL::character varying NULL,
	availablespotnumber_md varchar(100) DEFAULT NULL::character varying NULL,
	totalspotnumber varchar(100) DEFAULT NULL::character varying NULL,
	totalspotnumber_md varchar(100) DEFAULT NULL::character varying NULL,
	availablespotpercentage varchar(100) NULL,
	availablespotpercentage_md varchar(100) NULL,
	operationalstatus varchar(100) NULL,
	operationalstatus_md varchar(100) NULL,
	inactive varchar(100) NULL,
	inactive_md varchar(100) NULL,
	CONSTRAINT trafico_any_offstreetparking_pkey PRIMARY KEY (recvtime, fiwareservicepath, entitytype, entityid)
);

create trigger insert_offstreetparking_to_aparcamientos before
insert
    on
    sc_vlci.eliminar_trafico_any_offstreetparking for each row execute procedure update_aparcamientos_realtime();


COMMIT;