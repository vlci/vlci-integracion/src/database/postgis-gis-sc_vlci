-- Revert gis-sc_vlci:v_migrations/1666-borrar-tabla-pointofinterest from pg

BEGIN;

CREATE TABLE sc_vlci.medioambiente_any_pointofinterest (
	recvtime timestamp NULL,
	fiwareservicepath text NULL,
	entityid varchar(64) NOT NULL,
	entitytype text NULL,
	bind_no int4 NULL,
	bind_no2 int4 NULL,
	bind_nox int4 NULL,
	bind_o3 int4 NULL,
	bind_pm1 int4 NULL,
	bind_pm10 int4 NULL,
	bind_pm25 int4 NULL,
	bind_so2 int4 NULL,
	bind_atmosphericpressure int4 NULL,
	bind_precipitation int4 NULL,
	bind_relativehumidity int4 NULL,
	bind_temperature int4 NULL,
	bind_winddirection int4 NULL,
	bind_windspeed int4 NULL,
	address text NULL,
	category text NULL,
	dataprovider text NULL,
	description text NULL,
	"location" text NULL,
	"name" text NULL,
	"owner" text NULL,
	bind_no_md text NULL,
	bind_no2_md text NULL,
	bind_nox_md text NULL,
	bind_o3_md text NULL,
	bind_pm1_md text NULL,
	bind_pm10_md text NULL,
	bind_pm25_md text NULL,
	bind_so2_md text NULL,
	bind_atmosphericpressure_md text NULL,
	bind_precipitation_md text NULL,
	bind_relativehumidity_md text NULL,
	bind_temperature_md text NULL,
	bind_winddirection_md text NULL,
	bind_windspeed_md text NULL,
	address_md text NULL,
	category_md text NULL,
	dataprovider_md text NULL,
	description_md text NULL,
	location_md text NULL,
	name_md text NULL,
	owner_md text NULL
);

INSERT INTO sc_vlci.medioambiente_any_pointofinterest (recvtime,fiwareservicepath,entityid,entitytype,bind_no,bind_no2,bind_nox,bind_o3,bind_pm1,bind_pm10,bind_pm25,bind_so2,bind_atmosphericpressure,bind_precipitation,bind_relativehumidity,bind_temperature,bind_winddirection,bind_windspeed,address,category,dataprovider,description,"location","name","owner",bind_no_md,bind_no2_md,bind_nox_md,bind_o3_md,bind_pm1_md,bind_pm10_md,bind_pm25_md,bind_so2_md,bind_atmosphericpressure_md,bind_precipitation_md,bind_relativehumidity_md,bind_temperature_md,bind_winddirection_md,bind_windspeed_md,address_md,category_md,dataprovider_md,description_md,location_md,name_md,owner_md) VALUES
	 ('2021-10-14 10:01:24.977','/MedioAmbiente','W01_AVFRANCIA','PointOfInterest',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1542,1543,1541,1540,782,783,NULL,'61',NULL,'Estación de clima de la Avenida de Francia.','01010000006ADFDC5F3DEED5BFEC88433690BA4340','AVDA.FRANCIA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]',NULL,'[]',NULL,'[]','[]','[]',NULL),
	 ('2021-10-14 10:01:24.977','/MedioAmbiente','W02_NAZARET','PointOfInterest',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1307,1308,1305,1304,1289,1099,NULL,'61',NULL,'Estación de clima de Nazaret.','01010000000E9F7422C154D5BF001FBC7669B94340','NAZARET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]',NULL,'[]',NULL,'[]','[]','[]',NULL),
	 ('2022-01-21 12:52:24.85','/MedioAmbiente','A01_AVFRANCIA','PointOfInterest',NULL,775,NULL,772,NULL,784,785,770,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'61','BSG','Estación de contaminación de la Avenida de Francia.','010100000099CD887725EED5BF9357E71890BA4340','Av.França',NULL,NULL,'[]',NULL,'[]',NULL,'[]','[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]',NULL),
	 ('2022-01-21 12:52:26.91','/MedioAmbiente','A02_BULEVARDSUD','PointOfInterest',NULL,695,NULL,698,NULL,NULL,NULL,697,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'61','BSG','Estación de contaminación del Bulevard Sud.','010100000053358B61985DD9BFE61F7D93A6B94340','Bulevard Sud',NULL,NULL,'[]',NULL,'[]',NULL,NULL,NULL,'[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]',NULL),
	 ('2022-01-21 12:52:29.691','/MedioAmbiente','A03_MOLISOL','PointOfInterest',NULL,209,NULL,206,NULL,210,211,204,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'61','BSG','Estación de contaminación Molí del Sol.','01010000000EBB945FAB29DABFFBB37AE294BD4340','Molí del sol',NULL,NULL,'[]',NULL,'[]',NULL,'[]','[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]',NULL),
	 ('2022-01-21 12:52:32.405','/MedioAmbiente','A04_PISTASILLA','PointOfInterest',NULL,43,NULL,40,NULL,54,157,38,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'61','BSG','Estación de contaminación de la Pista de Silla.','0101000000A9392F04EF1AD8BFC58954BDA1BA4340','Pista Silla',NULL,NULL,'[]',NULL,'[]',NULL,'[]','[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]',NULL),
	 ('2022-01-21 12:52:34.564','/MedioAmbiente','A05_POLITECNIC','PointOfInterest',NULL,400,NULL,397,NULL,404,405,396,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'61','BSG','Estación de contaminación del Politécnico.','0101000000C270091BF997D5BF679AB0FD64BD4340','U. Politècnica',NULL,NULL,'[]',NULL,'[]',NULL,'[]','[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]',NULL),
	 ('2022-01-21 12:52:37.166','/MedioAmbiente','A06_VIVERS','PointOfInterest',NULL,194,NULL,197,NULL,NULL,NULL,190,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'61','BSG','Estación de contaminación del Parque de Viveros.','0101000000FA9D81EC50A8D7BF79A97DDF64BD4340','Vivers',NULL,NULL,'[]',NULL,'[]',NULL,NULL,NULL,'[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]',NULL),
	 ('2022-01-21 12:52:39.72','/MedioAmbiente','A07_VALENCIACENTRE','PointOfInterest',NULL,2849,NULL,NULL,NULL,2851,2852,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'61','BSG','Estación de contaminación del centro de Valencia.','0101000000B4BE92F5E516D8BFA05A33E83ABC4340','Centre',NULL,NULL,'[]',NULL,NULL,NULL,'[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]',NULL),
	 ('2022-01-21 12:52:42.539','/MedioAmbiente','A08_DR_LLUCH','PointOfInterest',3139,3140,3141,NULL,3144,3142,3143,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Calle Dr. Lluch, 48','49','BSG','Estación de medición de calidad del aire en el C.M.A.P.M. Canyamelar-Cabanyal','010100000096DFAE064402D5BF4A5472C5BABB4340','DR.LLUCH','Laboratorio Municipal','[]','[]','[]',NULL,'[]','[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]');
INSERT INTO sc_vlci.medioambiente_any_pointofinterest (recvtime,fiwareservicepath,entityid,entitytype,bind_no,bind_no2,bind_nox,bind_o3,bind_pm1,bind_pm10,bind_pm25,bind_so2,bind_atmosphericpressure,bind_precipitation,bind_relativehumidity,bind_temperature,bind_winddirection,bind_windspeed,address,category,dataprovider,description,"location","name","owner",bind_no_md,bind_no2_md,bind_nox_md,bind_o3_md,bind_pm1_md,bind_pm10_md,bind_pm25_md,bind_so2_md,bind_atmosphericpressure_md,bind_precipitation_md,bind_relativehumidity_md,bind_temperature_md,bind_winddirection_md,bind_windspeed_md,address_md,category_md,dataprovider_md,description_md,location_md,name_md,owner_md) VALUES
	 ('2022-01-21 12:52:44.947','/MedioAmbiente','A09_CABANYAL','PointOfInterest',3155,3156,3157,NULL,3160,3158,3159,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Carrer del Progrés, 379','49','BSG','Estación de medición de calidad del aire en el Cabanyal','0101000000297EBCF28106D5BF56886AF1B9BC4340','CABANYAL','Laboratorio Municipal','[]','[]','[]',NULL,'[]','[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]'),
	 ('2022-01-21 12:52:50.158','/MedioAmbiente','A11_PATRAIX','PointOfInterest',3424,3423,3425,NULL,NULL,3421,3422,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Av. de les Tres Creus amb Camí Nou de Picanya','49','DNota via BSG','Estación de contaminación del barrio de Patraix instalada por DNota como parte de los Presupuestos Participativos de 2021','01010000001403C9E0CD3FDABFB874CC79C6BA4340','PATRAIX','Laboratorio Municipal','[]','[]','[]',NULL,NULL,'[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]'),
	 ('2022-02-15 12:23:30.52','/MedioAmbiente','A10_OLIVERETA','PointOfInterest',3419,3418,3420,NULL,NULL,3416,3417,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Av. del Cid amb Av. de les Tres Creus amb Carrer del Nou d`Octubre','49','DNota via BSG','Estación de contaminación del barrio de l`Olivereta instalada por DNota como parte de los Presupuestos Participativos de 2021','{"type":"Point","coordinates":[-0.405895,39.469214]}','OLIVERETA','Laboratorio Municipal','[]','[]','[]',NULL,NULL,'[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]'),
	 ('2022-03-10 11:30:55.24','/MedioAmbiente','A10_OLIVERETA','PointOfInterest',3419,3418,NULL,NULL,NULL,3416,3417,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Av. del Cid amb Av. de les Tres Creus amb Carrer del Nou d`Octubre','49','DNota via BSG','Estación de contaminación del barrio de l`Olivereta instalada por DNota como parte de los Presupuestos Participativos de 2021','{"type":"Point","coordinates":[-0.405895,39.469214]}','OLIVERETA','Laboratorio Municipal','[]','[]',NULL,NULL,NULL,'[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]'),
	 ('2022-07-20 11:05:11.463','/MedioAmbiente','A08_DR_LLUCH','PointOfInterest',3139,3140,3141,NULL,3144,3142,3143,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Calle Dr. Lluch, 48','49','BSG','Estación de medición de calidad del aire en el C.M.A.P.M. Canyamelar-Cabanyal','{"type":"Point","coordinates":[-0.328263289,39.466637307]}','Dr. Lluch','Laboratorio Municipal','[]','[]','[]',NULL,'[]','[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]'),
	 ('2022-07-20 11:05:45.391','/MedioAmbiente','A10_OLIVERETA','PointOfInterest',3419,3418,NULL,NULL,NULL,3416,3417,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Av. del Cid amb Av. de les Tres Creus amb Carrer del Nou d`Octubre','49','DNota via BSG','Estación de contaminación del barrio de l`Olivereta instalada por DNota como parte de los Presupuestos Participativos de 2021','{"type":"Point","coordinates":[-0.405895,39.469214]}','Olivereta','Laboratorio Municipal','[]','[]',NULL,NULL,NULL,'[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]'),
	 ('2022-07-20 11:06:23.468','/MedioAmbiente','A11_PATRAIX','PointOfInterest',3424,3423,3425,NULL,NULL,3421,3422,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Av. de les Tres Creus amb Camí Nou de Picanya','49','DNota via BSG','Estación de contaminación del barrio de Patraix instalada por DNota como parte de los Presupuestos Participativos de 2021','{"type":"Point","coordinates":[-0.4101443,39.459182]}','Patraix','Laboratorio Municipal','[]','[]','[]',NULL,NULL,'[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]'),
	 ('2022-07-20 11:06:39.674','/MedioAmbiente','A09_CABANYAL','PointOfInterest',3155,3156,3157,NULL,3160,3158,3159,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Carrer del Progrés, 379','49','BSG','Estación de medición de calidad del aire en el Cabanyal','{"type":"Point","coordinates":[-0.328522193,39.474424531]}','Cabanyal','Laboratorio Municipal','[]','[]','[]',NULL,'[]','[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]'),
	 ('2023-03-24 11:58:28.273','/MedioAmbiente','A11_PATRAIX','PointOfInterest',NULL,3423,3425,NULL,NULL,3421,3422,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Av. de les Tres Creus amb Camí Nou de Picanya','49','DNota via BSG','Estación de contaminación del barrio de Patraix instalada por DNota como parte de los Presupuestos Participativos de 2021','{"type":"Point","coordinates":[-0.4101443,39.459182]}','Patraix','Laboratorio Municipal',NULL,'[]','[]',NULL,NULL,'[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]'),
	 ('2023-03-24 11:58:28.282','/MedioAmbiente','A11_PATRAIX','PointOfInterest',NULL,NULL,3425,NULL,NULL,3421,3422,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Av. de les Tres Creus amb Camí Nou de Picanya','49','DNota via BSG','Estación de contaminación del barrio de Patraix instalada por DNota como parte de los Presupuestos Participativos de 2021','{"type":"Point","coordinates":[-0.4101443,39.459182]}','Patraix','Laboratorio Municipal',NULL,NULL,'[]',NULL,NULL,'[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]');
INSERT INTO sc_vlci.medioambiente_any_pointofinterest (recvtime,fiwareservicepath,entityid,entitytype,bind_no,bind_no2,bind_nox,bind_o3,bind_pm1,bind_pm10,bind_pm25,bind_so2,bind_atmosphericpressure,bind_precipitation,bind_relativehumidity,bind_temperature,bind_winddirection,bind_windspeed,address,category,dataprovider,description,"location","name","owner",bind_no_md,bind_no2_md,bind_nox_md,bind_o3_md,bind_pm1_md,bind_pm10_md,bind_pm25_md,bind_so2_md,bind_atmosphericpressure_md,bind_precipitation_md,bind_relativehumidity_md,bind_temperature_md,bind_winddirection_md,bind_windspeed_md,address_md,category_md,dataprovider_md,description_md,location_md,name_md,owner_md) VALUES
	 ('2023-03-24 11:58:28.286','/MedioAmbiente','A11_PATRAIX','PointOfInterest',NULL,NULL,NULL,NULL,NULL,3421,3422,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Av. de les Tres Creus amb Camí Nou de Picanya','49','DNota via BSG','Estación de contaminación del barrio de Patraix instalada por DNota como parte de los Presupuestos Participativos de 2021','{"type":"Point","coordinates":[-0.4101443,39.459182]}','Patraix','Laboratorio Municipal',NULL,NULL,NULL,NULL,NULL,'[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]'),
	 ('2023-05-02 06:12:26.669','/MedioAmbiente','A11_PATRAIX','PointOfInterest',3424,3423,3425,NULL,NULL,3421,3422,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Av. de les Tres Creus amb Camí Nou de Picanya','49','DNota via BSG','Estación de contaminación del barrio de Patraix instalada por DNota como parte de los Presupuestos Participativos de 2021','{"type":"Point","coordinates":[-0.4101443,39.459182]}','Patraix','Laboratorio Municipal','[]','[]','[]',NULL,NULL,'[]','[]',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]','[]'),
	 ('2023-06-02 10:30:03.584','/MedioAmbiente','W01_AVFRANCIA','PointOfInterest',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1543,1543,1541,1540,782,783,NULL,'61',NULL,'Estación de clima de la Avenida de Francia.','{"type":"Point","coordinates":[-0.342666,39.457526]}','AVDA.FRANCIA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]',NULL,'[]',NULL,'[]','[]','[]',NULL),
	 ('2023-06-02 10:30:10.855','/MedioAmbiente','W01_AVFRANCIA','PointOfInterest',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1542,1543,1541,1540,782,783,NULL,'61',NULL,'Estación de clima de la Avenida de Francia.','{"type":"Point","coordinates":[-0.342666,39.457526]}','AVDA.FRANCIA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'[]','[]','[]','[]','[]','[]',NULL,'[]',NULL,'[]','[]','[]',NULL);


COMMIT;
