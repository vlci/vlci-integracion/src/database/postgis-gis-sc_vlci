-- Revert gis-sc_vlci:v_migrations/3136.Eliminar-tablas-vacias-gis from pg

BEGIN;

CREATE TABLE sc_vlci.medioambiente_any_weatherobserved (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(100) NOT NULL,
	entitytype varchar(100) NOT NULL,
	entityid varchar(100) NOT NULL,
	dateobserved timestamp NULL,
	dateobserved_md varchar(100) NULL DEFAULT NULL::character varying,
	temperaturevalue varchar(100) NULL DEFAULT NULL::character varying,
	temperaturevalue_md varchar(100) NULL DEFAULT NULL::character varying,
	precipitationvalue varchar(100) NULL DEFAULT NULL::character varying,
	precipitationvalue_md varchar(100) NULL DEFAULT NULL::character varying,
	winddirectionvalue varchar(100) NULL DEFAULT NULL::character varying,
	winddirectionvalue_md varchar(100) NULL DEFAULT NULL::character varying,
	windspeedvalue varchar(100) NULL DEFAULT NULL::character varying,
	windspeedvalue_md varchar(100) NULL DEFAULT NULL::character varying,
	atmosphericpressurevalue varchar(100) NULL DEFAULT NULL::character varying,
	atmosphericpressurevalue_md varchar(100) NULL DEFAULT NULL::character varying,
	relativehumidityvalue varchar(100) NULL DEFAULT NULL::character varying,
	relativehumidityvalue_md varchar(100) NULL DEFAULT NULL::character varying,
	gis_id varchar(100) NULL DEFAULT NULL::character varying,
	gis_id_md varchar(100) NULL DEFAULT NULL::character varying,
	CONSTRAINT medioambiente_any_weatherobserved_pkey PRIMARY KEY (recvtime, fiwareservicepath, entitytype, entityid)
);
create trigger insert_weatherobserved_to_estatatmosfericas before
insert
    on
    sc_vlci.medioambiente_any_weatherobserved for each row execute procedure sc_vlci.update_estatatmosfericas_medidas();

CREATE TABLE sc_vlci.mercadoruzafa_aforo_casetasegur_2575_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	counter varchar(250) NULL,
	counter_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_aforo_casetasegur_2575_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_aforo_casetasegur after
insert
    on
    sc_vlci.mercadoruzafa_aforo_casetasegur_2575_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_aforo();

CREATE TABLE sc_vlci.mercadoruzafa_aforo_dentisana_2584_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	counter varchar(250) NULL,
	counter_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_aforo_dentisana_2584_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_aforo_dentisana after
insert
    on
    sc_vlci.mercadoruzafa_aforo_dentisana_2584_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_aforo();

CREATE TABLE sc_vlci.mercadoruzafa_aforo_entradaipetrona_2580_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	counter varchar(250) NULL,
	counter_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_aforo_entradaipetrona_2580_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_aforo_entradaipetrona after
insert
    on
    sc_vlci.mercadoruzafa_aforo_entradaipetrona_2580_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_aforo();

CREATE TABLE sc_vlci.mercadoruzafa_aforo_grupo90_2578_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	counter varchar(250) NULL,
	counter_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_aforo_grupo90_2578_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_aforo_grupo90 after
insert
    on
    sc_vlci.mercadoruzafa_aforo_grupo90_2578_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_aforo();

CREATE TABLE sc_vlci.mercadoruzafa_aforo_improntas_pescaext_2579_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	counter varchar(250) NULL,
	counter_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_aforo_improntas_pescaext_2579_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_aforo_improntas_pescaext after
insert
    on
    sc_vlci.mercadoruzafa_aforo_improntas_pescaext_2579_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_aforo();

CREATE TABLE sc_vlci.mercadoruzafa_aforo_mrgold_2583_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	counter varchar(250) NULL,
	counter_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_aforo_mrgold_2583_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_aforo_mrgold after
insert
    on
    sc_vlci.mercadoruzafa_aforo_mrgold_2583_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_aforo();

CREATE TABLE sc_vlci.mercadoruzafa_aforo_rosador_2585_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	counter varchar(250) NULL,
	counter_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_aforo_rosador_2585_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_aforo_rosador after
insert
    on
    sc_vlci.mercadoruzafa_aforo_rosador_2585_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_aforo();

CREATE TABLE sc_vlci.mercadoruzafa_aforo_trixka_2581_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	counter varchar(250) NULL,
	counter_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_aforo_trixka_2581_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_aforo_trixka after
insert
    on
    sc_vlci.mercadoruzafa_aforo_trixka_2581_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_aforo();

CREATE TABLE sc_vlci.mercadoruzafa_consumo_consumoaireacont_2552_e3t_lb40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	current1 varchar(250) NULL,
	current1_md varchar(250) NULL,
	current2 varchar(250) NULL,
	current2_md varchar(250) NULL,
	current3 varchar(250) NULL,
	current3_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	energy_3phase varchar(250) NULL,
	energy_3phase_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	power_active1 varchar(250) NULL,
	power_active1_md varchar(250) NULL,
	power_active2 varchar(250) NULL,
	power_active2_md varchar(250) NULL,
	power_active3 varchar(250) NULL,
	power_active3_md varchar(250) NULL,
	power_factor varchar(250) NULL,
	power_factor_md varchar(250) NULL,
	power_factor1 varchar(250) NULL,
	power_factor1_md varchar(250) NULL,
	power_factor2 varchar(250) NULL,
	power_factor2_md varchar(250) NULL,
	power_factor3 varchar(250) NULL,
	power_factor3_md varchar(250) NULL,
	power_reactive varchar(250) NULL,
	power_reactive_md varchar(250) NULL,
	power_reactive1 varchar(250) NULL,
	power_reactive1_md varchar(250) NULL,
	power_reactive2 varchar(250) NULL,
	power_reactive2_md varchar(250) NULL,
	power_reactive3 varchar(250) NULL,
	power_reactive3_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	voltage1 varchar(250) NULL,
	voltage1_md varchar(250) NULL,
	voltage2 varchar(250) NULL,
	voltage2_md varchar(250) NULL,
	voltage3 varchar(250) NULL,
	voltage3_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_consumo_consumoaireacont_2552_e3t_lb40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_consumo_aireacont after
insert
    on
    sc_vlci.mercadoruzafa_consumo_consumoaireacont_2552_e3t_lb40 for each row execute procedure sc_vlci.function_sensores_consumo();

CREATE TABLE sc_vlci.mercadoruzafa_consumo_consumogeneral_2551_e3t_lb40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	current1 varchar(250) NULL,
	current1_md varchar(250) NULL,
	current2 varchar(250) NULL,
	current2_md varchar(250) NULL,
	current3 varchar(250) NULL,
	current3_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	energy_3phase varchar(250) NULL,
	energy_3phase_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	power_active1 varchar(250) NULL,
	power_active1_md varchar(250) NULL,
	power_active2 varchar(250) NULL,
	power_active2_md varchar(250) NULL,
	power_active3 varchar(250) NULL,
	power_active3_md varchar(250) NULL,
	power_factor varchar(250) NULL,
	power_factor_md varchar(250) NULL,
	power_factor1 varchar(250) NULL,
	power_factor1_md varchar(250) NULL,
	power_factor2 varchar(250) NULL,
	power_factor2_md varchar(250) NULL,
	power_factor3 varchar(250) NULL,
	power_factor3_md varchar(250) NULL,
	power_reactive varchar(250) NULL,
	power_reactive_md varchar(250) NULL,
	power_reactive1 varchar(250) NULL,
	power_reactive1_md varchar(250) NULL,
	power_reactive2 varchar(250) NULL,
	power_reactive2_md varchar(250) NULL,
	power_reactive3 varchar(250) NULL,
	power_reactive3_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	voltage1 varchar(250) NULL,
	voltage1_md varchar(250) NULL,
	voltage2 varchar(250) NULL,
	voltage2_md varchar(250) NULL,
	voltage3 varchar(250) NULL,
	voltage3_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_consumo_consumogeneral_2551_e3t_lb40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_consumo_general after
insert
    on
    sc_vlci.mercadoruzafa_consumo_consumogeneral_2551_e3t_lb40 for each row execute procedure sc_vlci.function_sensores_consumo();

CREATE TABLE sc_vlci.mercadoruzafa_disuasorio_entradaipetrona_2574_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	presence varchar(250) NULL,
	presence_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_disuasorio_entradaipetrona_2574_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_disuasorio_entradaipetrona after
insert
    on
    sc_vlci.mercadoruzafa_disuasorio_entradaipetrona_2574_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_disuasorio();

CREATE TABLE sc_vlci.mercadoruzafa_disuasorio_grupo90_2576_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	presence varchar(250) NULL,
	presence_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_disuasorio_grupo90_2576_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_disuasorio_grupo90 after
insert
    on
    sc_vlci.mercadoruzafa_disuasorio_grupo90_2576_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_disuasorio();

CREATE TABLE sc_vlci.mercadoruzafa_disuasorio_improntas_pescaext_2586_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	presence varchar(250) NULL,
	presence_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_disuasorio_improntas_pescaext_2586_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_disuasorio_improntas_pescaext after
insert
    on
    sc_vlci.mercadoruzafa_disuasorio_improntas_pescaext_2586_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_disuasorio();

CREATE TABLE sc_vlci.mercadoruzafa_disuasorio_mrgold_2573_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	presence varchar(250) NULL,
	presence_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_disuasorio_mrgold_2573_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_disuasorio_mrgold after
insert
    on
    sc_vlci.mercadoruzafa_disuasorio_mrgold_2573_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_disuasorio();

CREATE TABLE sc_vlci.mercadoruzafa_ruido_2550_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	noise varchar(250) NULL,
	noise_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_ruido_2550_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_ruido after
insert
    on
    sc_vlci.mercadoruzafa_ruido_2550_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_ruido();

CREATE TABLE sc_vlci.mercadoruzafa_sensaciontermica_2553_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	humidity varchar(250) NULL,
	humidity_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	temperature varchar(250) NULL,
	temperature_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_sensaciontermica_2553_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_sensacion_termica_2553 after
insert
    on
    sc_vlci.mercadoruzafa_sensaciontermica_2553_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_sensacion_termica();

CREATE TABLE sc_vlci.mercadoruzafa_sensaciontermica_2554_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	humidity varchar(250) NULL,
	humidity_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	temperature varchar(250) NULL,
	temperature_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_sensaciontermica_2554_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_sensacion_termica_2554 after
insert
    on
    sc_vlci.mercadoruzafa_sensaciontermica_2554_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_sensacion_termica();

CREATE TABLE sc_vlci.mercadoruzafa_sensaciontermica_2555_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	humidity varchar(250) NULL,
	humidity_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	temperature varchar(250) NULL,
	temperature_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_sensaciontermica_2555_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_sensacion_termica_2555 after
insert
    on
    sc_vlci.mercadoruzafa_sensaciontermica_2555_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_sensacion_termica();

CREATE TABLE sc_vlci.mercadoruzafa_sensaciontermica_2556_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	humidity varchar(250) NULL,
	humidity_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	temperature varchar(250) NULL,
	temperature_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_sensaciontermica_2556_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_sensacion_termica_2556 after
insert
    on
    sc_vlci.mercadoruzafa_sensaciontermica_2556_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_sensacion_termica();

CREATE TABLE sc_vlci.mercadoruzafa_sensaciontermica_temperaturapanel_2557_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	humidity varchar(250) NULL,
	humidity_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	temperature varchar(250) NULL,
	temperature_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_sensaciontermica_temperaturapanel_2557_e3t_l_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_sensacion_termica_temperaturapanel after
insert
    on
    sc_vlci.mercadoruzafa_sensaciontermica_temperaturapanel_2557_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_sensacion_termica();

CREATE TABLE sc_vlci.mercadoruzafa_sensaciontermica_temppescaderia_2558_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	humidity varchar(250) NULL,
	humidity_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	temperature varchar(250) NULL,
	temperature_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT mercadoruzafa_sensaciontermica_temppescaderia_2558_e3t_ls4_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_sensacion_termica_temppescaderia after
insert
    on
    sc_vlci.mercadoruzafa_sensaciontermica_temppescaderia_2558_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_sensacion_termica();

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_avdaalfauir_2604_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_avdaalfauir_2604_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_laboratorio_avdaalfauir after
insert
    on
    sc_vlci.radiacion_laboratoriomunicipal_avdaalfauir_2604_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_radiacion();

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_bomberoscecom_2608_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_bomberoscecom_2608_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_laboratorio_bomberoscecom after
insert
    on
    sc_vlci.radiacion_laboratoriomunicipal_bomberoscecom_2608_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_radiacion();

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_bomberossaler_2634_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_bomberossaler_2634_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_laboratorio_bomberossaler after
insert
    on
    sc_vlci.radiacion_laboratoriomunicipal_bomberossaler_2634_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_radiacion();

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_callealta_2609_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_callealta_2609_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_laboratorio_callealta after
insert
    on
    sc_vlci.radiacion_laboratoriomunicipal_callealta_2609_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_radiacion();

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_gulliver_2633_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_gulliver_2633_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_laboratorio_gulliver after
insert
    on
    sc_vlci.radiacion_laboratoriomunicipal_gulliver_2633_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_radiacion();

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_laboratorioavfranc_2610_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_laboratorioavfranc_2610_e3t_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_laboratorio_avfranc after
insert
    on
    sc_vlci.radiacion_laboratoriomunicipal_laboratorioavfranc_2610_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_radiacion();

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_molidelsol_2601_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_molidelsol_2601_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_laboratorio_molidelsol after
insert
    on
    sc_vlci.radiacion_laboratoriomunicipal_molidelsol_2601_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_radiacion();

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_naturia_2607_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_naturia_2607_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_laboratorio_naturia after
insert
    on
    sc_vlci.radiacion_laboratoriomunicipal_naturia_2607_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_radiacion();

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_parquebombcampanar_2605_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_parquebombcampanar_2605_e3t_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_laboratorio_parquebombcampanar after
insert
    on
    sc_vlci.radiacion_laboratoriomunicipal_parquebombcampanar_2605_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_radiacion();

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_playamalva_2623_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_playamalva_2623_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_laboratorio_playamalva after
insert
    on
    sc_vlci.radiacion_laboratoriomunicipal_playamalva_2623_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_radiacion();

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_policiaalfara_2635_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_policiaalfara_2635_e3t_ls40_pkey PRIMARY KEY (recvtime)
);

CREATE TABLE sc_vlci.radiacion_laboratoriomunicipal_policiamaritimo_2606_e3t_ls40 (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(250) NULL,
	entitytype varchar(250) NULL,
	entityid varchar(250) NULL,
	uvindex varchar(250) NULL,
	uvindex_md varchar(250) NULL,
	"connection" varchar(250) NULL,
	connection_md varchar(250) NULL,
	"current" varchar(250) NULL,
	current_md varchar(250) NULL,
	dimmer varchar(250) NULL,
	dimmer_md varchar(250) NULL,
	energy varchar(250) NULL,
	energy_md varchar(250) NULL,
	measures_date_last varchar(250) NULL,
	measures_date_last_md varchar(250) NULL,
	"mode" varchar(250) NULL,
	mode_md varchar(250) NULL,
	"position" varchar(250) NULL,
	position_md varchar(250) NULL,
	power_active varchar(250) NULL,
	power_active_md varchar(250) NULL,
	status varchar(250) NULL,
	status_md varchar(250) NULL,
	voltage varchar(250) NULL,
	voltage_md varchar(250) NULL,
	CONSTRAINT radiacion_laboratoriomunicipal_policiamaritimo_2606_e3t_ls_pkey PRIMARY KEY (recvtime)
);

-- Table Triggers

create trigger trigger_sensor_laboratorio_policiamaritimo after
insert
    on
    sc_vlci.radiacion_laboratoriomunicipal_policiamaritimo_2606_e3t_ls40 for each row execute procedure sc_vlci.function_sensores_radiacion();

CREATE TABLE sc_vlci.residuos_alcaldia_any_keyperformanceindicator (
	recvtime timestamp NOT NULL,
	fiwareservicepath varchar(100) NOT NULL,
	entitytype varchar(100) NOT NULL,
	entityid varchar(100) NOT NULL,
	calculationperiod varchar(100) NULL DEFAULT NULL::character varying,
	calculationperiod_md varchar(100) NULL DEFAULT NULL::character varying,
	kpivalue varchar(100) NULL DEFAULT NULL::character varying,
	kpivalue_md varchar(100) NULL DEFAULT NULL::character varying,
	"name" varchar(100) NULL DEFAULT NULL::character varying,
	name_md varchar(100) NULL DEFAULT NULL::character varying,
	updatedat varchar(100) NULL DEFAULT NULL::character varying,
	updatedat_md varchar(100) NULL DEFAULT NULL::character varying,
	sliceanddicevalue1 varchar(100) NULL DEFAULT NULL::character varying,
	sliceanddicevalue1_md varchar(100) NULL DEFAULT NULL::character varying,
	sliceanddicevalue2 varchar(100) NULL DEFAULT NULL::character varying,
	sliceanddicevalue2_md varchar(100) NULL DEFAULT NULL::character varying,
	CONSTRAINT residuos_alcaldia_any_keyperformanceindicator_pkey PRIMARY KEY (recvtime, fiwareservicepath, entitytype, entityid)
);

CREATE TABLE sc_vlci.sc_vlci_error_log (
	"timestamp" timestamp NULL,
	error text NULL,
	query text NULL
);

COMMIT;
