-- Revert gis-sc_vlci:v_migrations/1841-datos-anomalos-EMT-072023 from pg

BEGIN;

INSERT INTO emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-04 20:34:04.927','/emt','KeyPerformanceIndicator','mob006-01','2023-07-03','[]',1371,'[]','Titulo','[]','EMTicket','[]','L','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-04 20:34:06.46','/emt','KeyPerformanceIndicator','mob006-04','2023-07-03','[]',1371,'[]','Ruta','[]',NULL,'[]','L','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	('2023-07-29 20:34:05.341','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28','[]',2,'[]','Titulo','[]','Abono Especial','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-29 20:34:06.603','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28','[]',4,'[]','Titulo','[]','Pase Empleado EMT','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-29 20:34:06.876','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28','[]',2,'[]','Titulo','[]','SUMA T-2+','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-29 20:34:06.932','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28','[]',3,'[]','Titulo','[]','SUMA 10','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-29 20:34:06.999','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28','[]',1,'[]','Titulo','[]','SUMA Mensual','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-29 20:34:07.26','/emt','KeyPerformanceIndicator','mob006-01','2023-07-28','[]',1,'[]','Titulo','[]','Billete Ordinario','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-29 20:34:07.324','/emt','KeyPerformanceIndicator','mob006-04','2023-07-28','[]',13,'[]','Ruta','[]','1','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-30 20:34:04.906','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',1,'[]','Titulo','[]','Familiar Empleado EMT','[]','S','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-30 20:34:06.583','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',78,'[]','Titulo','[]','Abono Especial','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-30 20:34:06.653','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',1,'[]','Titulo','[]','Pase Empleado EMT','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-30 20:34:06.726','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',4,'[]','Titulo','[]','BB Personalizado General','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-30 20:34:06.99','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',24,'[]','Titulo','[]','Amb Tu','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-30 20:34:08.145','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',59,'[]','Titulo','[]','SUMA 10','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-30 20:34:08.208','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',1,'[]','Titulo','[]','BB Personalizado Especial','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-30 20:34:08.464','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',2,'[]','Titulo','[]','SUMA Mensual','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-30 20:34:08.73','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',107,'[]','Titulo','[]','BonoOro','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-30 20:34:08.998','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',52,'[]','Titulo','[]','Bonobús','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-30 20:34:09.54','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',9,'[]','Titulo','[]','Billete Ordinario','[]','S','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-30 20:34:09.11','/emt','KeyPerformanceIndicator','mob006-01','2023-07-29','[]',6,'[]','Titulo','[]','Bono Infantil','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-30 20:34:09.172','/emt','KeyPerformanceIndicator','mob006-04','2023-07-29','[]',250,'[]','Ruta','[]','70','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-30 20:34:09.229','/emt','KeyPerformanceIndicator','mob006-04','2023-07-29','[]',94,'[]','Ruta','[]','40','[]','S','[]',NULL,NULL,NULL,NULL);


COMMIT;
