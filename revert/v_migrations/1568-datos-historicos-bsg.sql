-- Revert gis-sc_vlci:v_migrations/1568-datos-historicos-bsg from pg

BEGIN;

delete from sc_vlci.medioambiente_any_airqualityobserved
where dateobserved < '2021-06-03 00:00:00' 
and dateobserved >= '2020-01-01 00:00:00';

COMMIT;
