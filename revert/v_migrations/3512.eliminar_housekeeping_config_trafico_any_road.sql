-- Revert gis-sc_vlci:v_migrations/3512.eliminar_housekeeping_config_trafico_any_road from pg
BEGIN;

INSERT INTO
  sc_vlci.housekeeping_config (table_nam, tiempo, colfecha)
VALUES
  ('trafico_any_road', 1, 'recvtime');

COMMIT;