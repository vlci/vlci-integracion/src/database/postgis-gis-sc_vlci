-- Revert gis-sc_vlci:v_migrations/1759-insertar-datos-emt from pg

BEGIN;

delete from sc_vlci.emt_any_keyperformanceindicator 
    where  sliceanddice1 in('Titulo', 'Ruta') and 
        calculationperiod in ('2023-06-09', '2023-06-10', '2023-06-11');

COMMIT;
