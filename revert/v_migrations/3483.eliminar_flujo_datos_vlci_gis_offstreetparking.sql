-- Revert gis-sc_vlci:v_migrations/3483.eliminar_flujo_datos_vlci_gis_offstreetparking from pg
BEGIN;

CREATE
OR REPLACE FUNCTION sc_vlci.update_aparcamientos_realtime () RETURNS TRIGGER LANGUAGE PLPGSQL AS $function$
DECLARE
var_estado VARCHAR;
var_plazaslibr INTEGER;
BEGIN
		CASE
			WHEN NEW.inactive = 'true' THEN
			var_estado := 'sinDatosPermanente';
			var_plazaslibr := -1;
			WHEN NEW.inactive = 'false' AND NEW.operationalStatus = 'noData' THEN
			var_estado := 'sinDatos';
			var_plazaslibr := -2;
			else
			var_estado := 'ok';
			var_plazaslibr := cast(NEW.availablespotnumber AS INTEGER);

		END CASE;

		CASE
			WHEN var_estado = 'ok' AND cast(NEW.availablespotpercentage AS NUMERIC) > 25 THEN
			var_estado := 'verde';
			WHEN var_estado = 'ok' AND cast(NEW.availablespotpercentage AS NUMERIC) >= 10 AND cast(NEW.availablespotpercentage AS NUMERIC) <=25 THEN
			var_estado := 'amarillo';
			WHEN var_estado = 'ok' AND cast(NEW.availablespotpercentage AS NUMERIC) < 10 THEN
			var_estado := 'rojo';
			else

		END CASE;

		UPDATE gis.tra_aparcamientos_evw SET
		plazaslibr= var_plazaslibr,
		plazastota= cast(NEW.totalspotnumber AS INTEGER),
		ultima_mod= cast(NEW.recvtime AS TIMESTAMP without time zone),
		usuario_mod= 'traficoVLCi',
		estado= var_estado
		WHERE id_aparcamiento=cast(NEW.idaparcamiento AS NUMERIC);
		RETURN NEW;
END;
$function$;

COMMIT;