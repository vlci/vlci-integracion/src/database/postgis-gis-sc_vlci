-- Revert gis-sc_vlci:v_migrations/2305.delete-isVisibleVMinut from pg

BEGIN;

ALTER TABLE sc_vlci.t_env_medioambiente_visualizacion_estaciones
ADD COLUMN isVisibleVMinut boolean NOT NULL DEFAULT TRUE;

UPDATE sc_vlci.t_env_medioambiente_visualizacion_estaciones
SET isVisibleVMinut =
  CASE 
    WHEN entityid IN ('A01_AVFRANCIA', 'A02_BULEVARDSUD', 'A03_MOLISOL', 'A04_PISTASILLA', 'A05_POLITECNIC', 'A06_VIVERS', 'A07_VALENCIACENTRE', 'A08_DR_LLUCH', 'A10_OLIVERETA', 'A11_PATRAIX') THEN true
    WHEN entityid = 'A09_CABANYAL' THEN false
  END;

COMMIT;
