-- Revert gis-sc_vlci:v_migrations/3480.eliminar_modificar_tablas_funciones_no_utilizadas from pg

BEGIN;

CREATE OR REPLACE FUNCTION sc_vlci.function_sensores_aforo()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE v_oldvalue int;
DECLARE v_oldvalue_entradas int;
DECLARE v_oldvalue_salidas int;
DECLARE v_oldvalue_acumulado int;
DECLARE v_newvalue int;
DECLARE v_id int;
DECLARE v_suma int;
DECLARE v_entradas int;
DECLARE v_salidas int;
DECLARE v_acumulados int;
-- DECLARE v_total int;
BEGIN
    v_id := (select right(new.entityid,position('_' in reverse(new.entityid))-1)::int);
	v_oldvalue := (select contador from gis.sensores_aforo where id = v_id);
    v_oldvalue_entradas := (select entradas from gis.sensores_aforo where id = v_id);
    v_oldvalue_salidas := (select salidas from gis.sensores_aforo where id = v_id);
    v_oldvalue_acumulado := (select acumulado from gis.sensores_aforo where id = v_id);
	v_newvalue := new.counter::int;
	v_suma := v_oldvalue + v_newvalue;

    IF v_newvalue > 0 THEN
        v_entradas := v_oldvalue_entradas + v_newvalue;
        v_acumulados := v_oldvalue_salidas + v_entradas;

        UPDATE gis.sensores_aforo
        SET entradas = v_entradas
        WHERE id = v_id;
    ELSIF v_newvalue < 0 THEN
        v_salidas := v_oldvalue_salidas + (v_newvalue * -1);
        v_acumulados := v_oldvalue_entradas + v_salidas;

        UPDATE gis.sensores_aforo 
        SET salidas = v_salidas
        WHERE id = v_id;
    ELSE
        v_acumulados := v_oldvalue_acumulado;
    END IF;

    UPDATE gis.sensores_aforo 
    SET contador = v_suma, acumulado = v_acumulados
    WHERE id = v_id;

    -- v_total := (select sum(contador) from gis.sensores_aforo where nombre != 'Aforo_Total');

    -- UPDATE gis.sensores_aforo 
    -- SET contador = v_total
    -- WHERE nombre = 'Aforo_Total';

           RETURN new;
END;
$function$
;


CREATE OR REPLACE FUNCTION sc_vlci.function_sensores_consumo()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE v_id int;
BEGIN
    v_id := (select right(new.entityid,position('_' in reverse(new.entityid))-1)::int);

    UPDATE gis.sensores_consumo
    SET potencia_activa = new.power_active::real, 
    	potencia_activa1 = new.power_active1::real,
    	potencia_activa2 = new.power_active2::real,
    	potencia_activa3 = new.power_active3::real,
    	potencia_factor = new.power_factor::real,
    	potencia_factor1 = new.power_factor1::real,
    	potencia_factor2 = new.power_factor2::real,
    	potencia_factor3 = new.power_factor3::real,
    	potencia_reactiva = new.power_reactive::real,
    	potencia_reactiva1 = new.power_reactive1::real,
    	potencia_reactiva2 = new.power_reactive2::real,
    	potencia_reactiva3 = new.power_reactive3::real,
        energia_fase3 = new.energy_3phase::real
    WHERE id = v_id;

           RETURN new;
END;
$function$
;


CREATE OR REPLACE FUNCTION sc_vlci.function_sensores_disuasorio()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE v_id int;
BEGIN
    v_id := (select right(new.entityid,position('_' in reverse(new.entityid))-1)::int);

    UPDATE gis.sensores_disuasorio
    SET presencia = new.presence::int
    WHERE id = v_id;

           RETURN new;
END;
$function$
;


CREATE OR REPLACE FUNCTION sc_vlci.function_sensores_radiacion()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE v_id int;
BEGIN
    v_id := (select right(new.entityid,position('_' in reverse(new.entityid))-1)::int);

    UPDATE gis.sensores_radiacion_solar
    SET uvindex = new.uvindex::real
    WHERE id = v_id;

           RETURN new;
END;
$function$
;


CREATE OR REPLACE FUNCTION sc_vlci.function_sensores_ruido()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE v_id int;
BEGIN
    v_id := (select right(new.entityid,position('_' in reverse(new.entityid))-1)::int);

    UPDATE gis.sensores_ruido
    SET ruido = new.noise::real
    WHERE id = v_id;

           RETURN new;
END;
$function$
;


CREATE OR REPLACE FUNCTION sc_vlci.function_sensores_sensacion_termica()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE v_id int;
BEGIN
    v_id := (select right(new.entityid,position('_' in reverse(new.entityid))-1)::int);

    UPDATE gis.sensores_sensacion_termica
    SET humedad = new.humidity::real,
    	temperatura = new.temperature::real
    WHERE id = v_id;

           RETURN new;
END;
$function$
;

CREATE OR REPLACE FUNCTION sc_vlci.update_sanitat_inserttimestamp()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
    IF new.entityId LIKE 'is-san-%' THEN
		NEW.inserttimestamp=now();
		RETURN NEW;
	END IF; 
END;
$function$
;


ALTER TABLE sc_vlci.eliminar_t_env_medioambiente_visualizacion_estaciones RENAME TO t_env_medioambiente_visualizacion_estaciones;

COMMIT;
