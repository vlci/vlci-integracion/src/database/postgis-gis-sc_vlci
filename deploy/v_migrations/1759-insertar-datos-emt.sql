-- Deploy gis-sc_vlci:v_migrations/1759-insertar-datos-emt to pg

BEGIN;

INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 07:35:51.219','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',86285,'[]','Titulo','[]','Abono Especial','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:52.514','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',892,'[]','Titulo','[]','Familiar Empleado EMT','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:52.68','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',649,'[]','Titulo','[]','Pase Empleado EMT','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:52.834','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',8,'[]','Titulo','[]','Pase Guardia Civil','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:54.145','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',429,'[]','Titulo','[]','EMV','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:54.327','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',1387,'[]','Titulo','[]','EMTicket','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:54.476','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',6643,'[]','Titulo','[]','Bono Infantil','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:54.642','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',194,'[]','Titulo','[]','SUMA T-1','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:54.985','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',20955,'[]','Titulo','[]','Amb Tu','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:55.124','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',1104,'[]','Titulo','[]','T Joven  ','[]','V','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 07:35:55.301','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',86,'[]','Titulo','[]','SUMA T-2','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:55.542','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',27,'[]','Titulo','[]','Valencia Card 3 dï¿œas','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:56.174','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',236,'[]','Titulo','[]','SUMA T-3','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:56.407','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',753,'[]','Titulo','[]','Abono Violeta','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:56.551','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',164,'[]','Titulo','[]','SUMA T-2+','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:56.674','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',1,'[]','Titulo','[]','ND','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:56.839','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',473,'[]','Titulo','[]','BB Personalizado Especial','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:57.535','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',105,'[]','Titulo','[]','Estudiante FGV','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:57.722','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',63,'[]','Titulo','[]','EMT Mascota','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:58.663','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',18085,'[]','Titulo','[]','Billete Ordinario','[]','V','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 07:35:58.926','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',1211,'[]','Titulo','[]','BB Personalizado General','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:59.283','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',73256,'[]','Titulo','[]','SUMA 10','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:59.439','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',208,'[]','Titulo','[]','SUMA Mensual Jove','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:59.599','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',14,'[]','Titulo','[]','Pase Evento','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:59.741','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',186,'[]','Titulo','[]','FGV Pase Empleado','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:35:59.921','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',128,'[]','Titulo','[]','Pensionista EMT','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:36:00.337','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',324,'[]','Titulo','[]','Pase Ayuntamiento','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:36:00.723','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',41463,'[]','Titulo','[]','Bonobï¿œs','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:36:00.877','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',2072,'[]','Titulo','[]','SUMA T-3+','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:36:01.39','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',122,'[]','Titulo','[]','SUMA T-1+','[]','V','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 07:36:01.976','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',8,'[]','Titulo','[]','Pase Ministerio Interior','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:36:02.355','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',25702,'[]','Titulo','[]','SUMA Mensual','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:36:02.571','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',65305,'[]','Titulo','[]','BonoOro','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:36:03.28','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',172,'[]','Titulo','[]','Familiar Pensionista EMT','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:36:03.415','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',22,'[]','Titulo','[]','Jubilado FGV','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:36:03.564','/emt','KeyPerformanceIndicator','mob006-01','2023-06-09','[]',6,'[]','Titulo','[]','Pase Estudiante EMT','[]','V','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 07:36:03.725','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',4413,'[]','Ruta','[]','12','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:03.894','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',1966,'[]','Ruta','[]','13','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:04.5','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',6364,'[]','Ruta','[]','35','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:04.394','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',4270,'[]','Ruta','[]','14','[]','V','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 07:36:04.592','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',7584,'[]','Ruta','[]','16','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:04.737','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',5483,'[]','Ruta','[]','18','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:04.863','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',9564,'[]','Ruta','[]','19','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:05.67','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',9494,'[]','Ruta','[]','4','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:05.26','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',10765,'[]','Ruta','[]','6','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:06.217','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',3850,'[]','Ruta','[]','7','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:06.481','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',5148,'[]','Ruta','[]','8','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:07.606','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',7805,'[]','Ruta','[]','81','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:07.892','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',11789,'[]','Ruta','[]','9','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:08.122','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',7130,'[]','Ruta','[]','60','[]','V','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 07:36:08.328','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',5217,'[]','Ruta','[]','62','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:08.464','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',4993,'[]','Ruta','[]','40','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:08.639','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',2923,'[]','Ruta','[]','63','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:08.813','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',11004,'[]','Ruta','[]','64','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:08.944','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',866,'[]','Ruta','[]','23','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:09.421','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',2950,'[]','Ruta','[]','67','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:09.612','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',1642,'[]','Ruta','[]','24','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:09.885','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',4058,'[]','Ruta','[]','25','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:10.292','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',1890,'[]','Ruta','[]','26','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:10.565','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',9262,'[]','Ruta','[]','27','[]','V','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 07:36:10.997','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',9125,'[]','Ruta','[]','28','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:11.176','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',15,'[]','Ruta','[]','98E','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:11.32','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',2671,'[]','Ruta','[]','C1','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:12.205','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',6102,'[]','Ruta','[]','C2','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:12.44','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',38234,'[]','Ruta','[]','C3','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:12.594','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',8582,'[]','Ruta','[]','70','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:12.843','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',16416,'[]','Ruta','[]','92','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:13.39','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',16243,'[]','Ruta','[]','93','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:13.272','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',10406,'[]','Ruta','[]','71','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:13.627','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',4210,'[]','Ruta','[]','94','[]','V','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 07:36:13.782','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',3971,'[]','Ruta','[]','72','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:14.127','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',5800,'[]','Ruta','[]','73','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:14.288','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',13351,'[]','Ruta','[]','95','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:14.472','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',1569,'[]','Ruta','[]','30','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:14.661','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',88,'[]','Ruta','[]','96','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:14.798','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',7791,'[]','Ruta','[]','31','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:14.94','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',8978,'[]','Ruta','[]','10','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:15.108','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',9325,'[]','Ruta','[]','32','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:15.487','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',7352,'[]','Ruta','[]','98','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 07:36:15.731','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',28929,'[]','Ruta','[]','99','[]','V','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 07:36:16.326','/emt','KeyPerformanceIndicator','mob006-04','2023-06-09','[]',9150,'[]','Ruta','[]','11','[]','V','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:30.72','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',53532,'[]','Titulo','[]','Abono Especial','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:31.263','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',489,'[]','Titulo','[]','Familiar Empleado EMT','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:33.451','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',444,'[]','Titulo','[]','Pase Empleado EMT','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:33.83','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',1,'[]','Titulo','[]','Pase Guardia Civil','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:33.959','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',367,'[]','Titulo','[]','EMV','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:34.148','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',1395,'[]','Titulo','[]','EMTicket','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:34.526','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',3952,'[]','Titulo','[]','Bono Infantil','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:34.743','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',148,'[]','Titulo','[]','SUMA T-1','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:35.549','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',12439,'[]','Titulo','[]','Amb Tu','[]','S','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:18:35.708','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',524,'[]','Titulo','[]','T Joven  ','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:36.93','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',93,'[]','Titulo','[]','SUMA T-2','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:36.549','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',27,'[]','Titulo','[]','Valencia Card 3 dï¿œas','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:36.689','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',419,'[]','Titulo','[]','Abono Violeta','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:37.62','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',180,'[]','Titulo','[]','SUMA T-3','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:37.205','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',329,'[]','Titulo','[]','SUMA T-2+','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:37.618','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',227,'[]','Titulo','[]','BB Personalizado Especial','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:37.894','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',43,'[]','Titulo','[]','Estudiante FGV','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:38.424','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',18138,'[]','Titulo','[]','Billete Ordinario','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:38.677','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',58,'[]','Titulo','[]','EMT Mascota','[]','S','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:18:39.7','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',504,'[]','Titulo','[]','BB Personalizado General','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:39.229','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',53747,'[]','Titulo','[]','SUMA 10','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:40.887','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',116,'[]','Titulo','[]','SUMA Mensual Jove','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:41.554','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',5,'[]','Titulo','[]','Pase Evento','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:41.72','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',98,'[]','Titulo','[]','FGV Pase Empleado','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:42.155','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',96,'[]','Titulo','[]','Pensionista EMT','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:42.524','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',24,'[]','Titulo','[]','Pase Ayuntamiento','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:42.731','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',25364,'[]','Titulo','[]','Bonobï¿œs','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:42.91','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',2126,'[]','Titulo','[]','SUMA T-3+','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:43.82','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',131,'[]','Titulo','[]','SUMA T-1+','[]','S','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:18:43.217','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',4,'[]','Titulo','[]','Pase Ministerio Interior','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:43.453','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',14257,'[]','Titulo','[]','SUMA Mensual','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:43.708','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',41506,'[]','Titulo','[]','BonoOro','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:43.925','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',110,'[]','Titulo','[]','Familiar Pensionista EMT','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:44.212','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',13,'[]','Titulo','[]','Jubilado FGV','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:44.398','/emt','KeyPerformanceIndicator','mob006-01','2023-06-10','[]',2,'[]','Titulo','[]','Pase Estudiante EMT','[]','S','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:18:44.604','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',2088,'[]','Ruta','[]','12','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:45.115','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',4932,'[]','Ruta','[]','35','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:45.444','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',1277,'[]','Ruta','[]','13','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:45.612','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',4149,'[]','Ruta','[]','14','[]','S','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:18:45.948','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',5592,'[]','Ruta','[]','16','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:46.287','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',2263,'[]','Ruta','[]','18','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:46.432','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',10529,'[]','Ruta','[]','19','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:46.627','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',5295,'[]','Ruta','[]','4','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:46.82','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',6774,'[]','Ruta','[]','6','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:47.95','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',2904,'[]','Ruta','[]','7','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:47.449','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',3737,'[]','Ruta','[]','8','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:47.591','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',5274,'[]','Ruta','[]','81','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:47.709','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',7900,'[]','Ruta','[]','9','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:47.877','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',6307,'[]','Ruta','[]','60','[]','S','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:18:48.115','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',3925,'[]','Ruta','[]','62','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:48.265','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',2274,'[]','Ruta','[]','40','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:48.451','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',7294,'[]','Ruta','[]','64','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:48.622','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',757,'[]','Ruta','[]','23','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:48.979','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',2192,'[]','Ruta','[]','67','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:49.152','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',1655,'[]','Ruta','[]','24','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:49.601','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',4415,'[]','Ruta','[]','25','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:49.994','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',1460,'[]','Ruta','[]','26','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:50.242','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',6708,'[]','Ruta','[]','27','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:50.411','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',537,'[]','Ruta','[]','E','[]','S','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:18:50.574','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',6579,'[]','Ruta','[]','28','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:50.73','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',14,'[]','Ruta','[]','98E','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:50.877','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',1775,'[]','Ruta','[]','C1','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:51.44','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',3637,'[]','Ruta','[]','C2','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:51.179','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',21448,'[]','Ruta','[]','C3','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:51.328','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',5953,'[]','Ruta','[]','70','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:51.684','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',10699,'[]','Ruta','[]','92','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:51.857','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',5574,'[]','Ruta','[]','71','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:52.105','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',7312,'[]','Ruta','[]','93','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:52.763','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',2611,'[]','Ruta','[]','72','[]','S','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:18:52.968','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',2894,'[]','Ruta','[]','94','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:53.186','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',8837,'[]','Ruta','[]','95','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:53.575','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',2857,'[]','Ruta','[]','73','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:53.737','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',69,'[]','Ruta','[]','96','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:53.949','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',1132,'[]','Ruta','[]','30','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:54.109','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',4249,'[]','Ruta','[]','31','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:54.491','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',4525,'[]','Ruta','[]','98','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:54.64','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',5779,'[]','Ruta','[]','10','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:54.828','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',9445,'[]','Ruta','[]','32','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:18:55.34','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',19128,'[]','Ruta','[]','99','[]','S','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:18:55.276','/emt','KeyPerformanceIndicator','mob006-04','2023-06-10','[]',6153,'[]','Ruta','[]','11','[]','S','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:04.755','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',36448,'[]','Titulo','[]','Abono Especial','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:06.381','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',202,'[]','Titulo','[]','Familiar Empleado EMT','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:06.791','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',327,'[]','Titulo','[]','Pase Empleado EMT','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:07.193','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',102,'[]','Titulo','[]','EMV','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:07.403','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',1057,'[]','Titulo','[]','EMTicket','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:07.67','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',2427,'[]','Titulo','[]','Bono Infantil','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:08.443','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',196,'[]','Titulo','[]','SUMA T-1','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:09.647','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',8166,'[]','Titulo','[]','Amb Tu','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:09.818','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',371,'[]','Titulo','[]','T Joven  ','[]','D','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:21:09.972','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',94,'[]','Titulo','[]','SUMA T-2','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:10.123','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',7,'[]','Titulo','[]','Valencia Card 3 dï¿œas','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:10.457','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',333,'[]','Titulo','[]','Abono Violeta','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:10.959','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',405,'[]','Titulo','[]','SUMA T-2+','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:11.314','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',197,'[]','Titulo','[]','SUMA T-3','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:11.619','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',142,'[]','Titulo','[]','BB Personalizado Especial','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:12.166','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',18,'[]','Titulo','[]','Estudiante FGV','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:12.333','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',13425,'[]','Titulo','[]','Billete Ordinario','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:12.461','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',48,'[]','Titulo','[]','EMT Mascota','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:12.885','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',262,'[]','Titulo','[]','BB Personalizado General','[]','D','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:21:13.96','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',36501,'[]','Titulo','[]','SUMA 10','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:13.246','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',100,'[]','Titulo','[]','SUMA Mensual Jove','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:13.608','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',1,'[]','Titulo','[]','Pase Evento','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:14.39','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',53,'[]','Titulo','[]','FGV Pase Empleado','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:15.107','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',66,'[]','Titulo','[]','Pensionista EMT','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:15.524','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',9,'[]','Titulo','[]','Pase Ayuntamiento','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:16.566','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',14739,'[]','Titulo','[]','Bonobï¿œs','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:17.36','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',1676,'[]','Titulo','[]','SUMA T-3+','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:17.17','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',99,'[]','Titulo','[]','SUMA T-1+','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:17.412','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',4,'[]','Titulo','[]','Pase Ministerio Interior','[]','D','[]','Viajes','[]','Viatges','[]');
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:21:18.95','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',9094,'[]','Titulo','[]','SUMA Mensual','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:18.655','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',3,'[]','Titulo','[]','Valencia Card 1 dï¿œa','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:19.23','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',33332,'[]','Titulo','[]','BonoOro','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:19.321','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',84,'[]','Titulo','[]','Familiar Pensionista EMT','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:19.627','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',10,'[]','Titulo','[]','Jubilado FGV','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:20.84','/emt','KeyPerformanceIndicator','mob006-01','2023-06-11','[]',1,'[]','Titulo','[]','Pase Estudiante EMT','[]','D','[]','Viajes','[]','Viatges','[]'),
	 ('2023-07-13 10:21:20.474','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',1731,'[]','Ruta','[]','12','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:20.633','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',3849,'[]','Ruta','[]','35','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:21.865','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',847,'[]','Ruta','[]','13','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:22.424','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',1754,'[]','Ruta','[]','14','[]','D','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:21:22.787','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',3525,'[]','Ruta','[]','16','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:22.956','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',1707,'[]','Ruta','[]','18','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:23.101','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',7243,'[]','Ruta','[]','19','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:23.271','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',4258,'[]','Ruta','[]','4','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:24.421','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',4552,'[]','Ruta','[]','6','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:24.862','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',1846,'[]','Ruta','[]','7','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:25.198','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',2407,'[]','Ruta','[]','8','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:25.331','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',4650,'[]','Ruta','[]','9','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:25.479','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',3000,'[]','Ruta','[]','81','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:25.628','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',3555,'[]','Ruta','[]','60','[]','D','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:21:27.21','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',2169,'[]','Ruta','[]','62','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:27.412','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',1317,'[]','Ruta','[]','40','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:27.885','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',4207,'[]','Ruta','[]','64','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:28.41','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',762,'[]','Ruta','[]','23','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:28.173','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',1333,'[]','Ruta','[]','67','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:29.305','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',1406,'[]','Ruta','[]','24','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:30.754','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',3258,'[]','Ruta','[]','25','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:32.11','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',1082,'[]','Ruta','[]','26','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:32.441','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',4893,'[]','Ruta','[]','27','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:32.82','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',542,'[]','Ruta','[]','E','[]','D','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:21:33.281','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',4274,'[]','Ruta','[]','28','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:33.545','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',15,'[]','Ruta','[]','98E','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:33.968','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',1588,'[]','Ruta','[]','C1','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:34.536','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',1928,'[]','Ruta','[]','C2','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:34.697','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',13571,'[]','Ruta','[]','C3','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:34.838','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',7647,'[]','Ruta','[]','92','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:35.58','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',3537,'[]','Ruta','[]','70','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:35.235','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',8850,'[]','Ruta','[]','93','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:35.439','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',3242,'[]','Ruta','[]','71','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:35.629','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',2189,'[]','Ruta','[]','94','[]','D','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:21:35.773','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',1880,'[]','Ruta','[]','72','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:36.139','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',6707,'[]','Ruta','[]','95','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:36.544','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',2099,'[]','Ruta','[]','73','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:36.694','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',809,'[]','Ruta','[]','30','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:37.35','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',41,'[]','Ruta','[]','96','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:37.758','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',3888,'[]','Ruta','[]','31','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:37.934','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',8068,'[]','Ruta','[]','32','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:38.882','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',3936,'[]','Ruta','[]','10','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:39.62','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',3205,'[]','Ruta','[]','98','[]','D','[]',NULL,NULL,NULL,NULL),
	 ('2023-07-13 10:21:39.959','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',12491,'[]','Ruta','[]','99','[]','D','[]',NULL,NULL,NULL,NULL);
INSERT INTO sc_vlci.emt_any_keyperformanceindicator (recvtime,fiwareservicepath,entitytype,entityid,calculationperiod,calculationperiod_md,kpivalue,kpivalue_md,sliceanddice1,sliceanddice1_md,sliceanddicevalue1,sliceanddicevalue1_md,diasemana,diasemana_md,measureunitcas,measureunitcas_md,measureunitval,measureunitval_md) VALUES
	 ('2023-07-13 10:21:40.179','/emt','KeyPerformanceIndicator','mob006-04','2023-06-11','[]',4141,'[]','Ruta','[]','11','[]','D','[]',NULL,NULL,NULL,NULL);


COMMIT;
