-- Deploy gis-sc_vlci:v_migrations/3480.eliminar_modificar_tablas_funciones_no_utilizadas to pg

BEGIN;

DROP FUNCTION IF EXISTS sc_vlci.function_sensores_aforo,
                          sc_vlci.function_sensores_consumo,
                          sc_vlci.function_sensores_disuasorio,
                          sc_vlci.function_sensores_radiacion,
                          sc_vlci.function_sensores_ruido,
                          sc_vlci.function_sensores_sensacion_termica,
                          sc_vlci.update_sanitat_inserttimestamp;

ALTER TABLE sc_vlci.t_env_medioambiente_visualizacion_estaciones RENAME TO eliminar_t_env_medioambiente_visualizacion_estaciones;

COMMIT;
