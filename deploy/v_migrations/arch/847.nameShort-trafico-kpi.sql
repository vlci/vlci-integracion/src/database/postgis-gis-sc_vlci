-- Deploy gis-sc_vlci:v_migrations/847.nameShort-trafico-kpi to pg

BEGIN;

ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator ADD nameshort varchar NULL;
ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator ADD nameshort_md varchar NULL;

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Accés Barcelona'
WHERE name='Accés Barcelona [entrada i eixida]';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Joan XXIII'
WHERE name='Prolongació Joan XXIII';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Av. del Cid'
WHERE name='Av. del Cid';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Pista de Silla'
WHERE name='Accés per V-31 [Pista de Silla]';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Corts Valencianes'
WHERE name='Corts Valencianes [Accés per CV-35]';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Arxiduc Carles'
WHERE name='Accés a Arxiduc Carles pel Camí nou de Picanya';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Ferran el Catòlic'
WHERE name='Gran Via de Ferran el Catòlic';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Blasco Ibáñez'
WHERE name='Av. Blasco Ibáñez';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Peris i Valero'
WHERE name='Av. de Peris i Valero';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Marqués del Túria'
WHERE name='Gran Via del Marqués del Túria';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Giorgeta'
WHERE name='Av. de Giorgeta';

UPDATE sc_vlci.trafico_any_keyperformanceindicator
SET nameshort='Dr. Peset Aleixandre'
WHERE name='Av. Dr. Peset Aleixandre';

COMMIT;
