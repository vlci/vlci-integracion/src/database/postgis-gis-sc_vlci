-- Deploy gis-sc_vlci:v_migrations/1560-eliminar-dias-no-completos to pg

BEGIN;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2021-02-01', '2020-05-31', '2020-06-01', '2020-12-02', '2021-03-06', '2021-05-02', '2021-06-21', '2021-10-31', '2021-11-01', '2021-11-13', '2021-11-14', '2022-02-22', '2022-03-30', '2022-04-10', '2022-06-05', '2023-02-13')
  and entityid IN ('Kpi-Accesos-Ciudad-Coches', 'Kpi-Accesos-Vias-Coches');

delete from trafico_any_keyperformanceindicator  k
where k.recvtime IN ('2021-06-02 23:30:20.210','2021-09-15 23:30:11.769');

delete from trafico_any_keyperformanceindicator  k
where k.recvtime = '2021-09-16 06:19:30.294' and k.calculationperiod = '2021-09-15' and k.entityid = 'Kpi-Accesos-Vias-Coches' and k.tramo = 'Av. de Peris i Valero [Entre Ausiàs March i Sapadors]'; 

delete from trafico_any_keyperformanceindicator  k
where k.recvtime = '2021-06-03 10:08:27.327' and k.calculationperiod = '2021-06-02' and k.entityid = 'Kpi-Accesos-Ciudad-Coches' and k.tramo = 'Prolongació Joan XXIII [Entre Germans Machado i Salvador Cerveró]';

COMMIT;
