-- Deploy gis-sc_vlci:v_migrations/769-crear-tabla-visualizacion_estaciones to pg

BEGIN;

CREATE TABLE sc_vlci.t_env_medioambiente_visualizacion_estaciones (
    entityid  VARCHAR(64)  NOT NULL,
    isVisibleVMinut boolean NOT NULL,
    postAtGeoportal boolean NOT NULL,
    aud_fec_ins  timestamptz,
    aud_user_ins  VARCHAR(100),
    aud_fec_upd  timestamptz,
    aud_user_upd  VARCHAR(100),
    PRIMARY KEY (entityid)
);

COMMIT;
