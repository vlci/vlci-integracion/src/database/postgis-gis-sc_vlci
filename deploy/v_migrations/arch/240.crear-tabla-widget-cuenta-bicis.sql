-- Deploy gis-sc_vlci:v_migrations/240.crear-tabla-widget-cuenta-bicis to pg

BEGIN;

CREATE TABLE sc_vlci.t_datos_cb_trafico_cuenta_bicis_lastdata (
    recvtime timestamptz,
    fiwareservicepath varchar(100),
    entityid varchar(100),
    entitytype varchar(100),
    description varchar(200),
    descriptionCas varchar(200),
    dateObserved varchar(100),
    dateObservedFrom varchar(100),
    dateObservedTo varchar(100),
    intensity int4,
    hourlyIntensity int4,
    location public."geometry",
    name varchar(200),
    operationalStatus varchar(10)
);

COMMIT;


