-- Deploy gis-sc_vlci:v_migrations/1581.recuperar-datos-faltantes-bicis to pg

BEGIN;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-05-31','2020-06-01','2020-12-02','2021-02-01','2021-03-27','2021-06-21','2021-07-05','2021-10-31','2021-11-01','2021-11-13','2021-11-14','2022-03-09','2022-03-30','2023-02-13')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo', 'Kpi-Trafico-Bicicletas-Anillo');

delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-19') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-21') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-21') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-22') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-23') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-24') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-24') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-25') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-27') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-30') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-01') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-03') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-10') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-11') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-14') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-19') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-20') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-21') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-22') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-23') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-30') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-06-22') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-06-23') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-06-24') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2021-09-03') and k.tramo = 'Pont de les arts'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-15') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-15') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-16') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-16') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-17') and k.tramo = 'Carrer Alacant'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-17') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-01') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-01') and k.tramo = 'Pont de fusta'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-01') and k.tramo = 'Pont del real'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-04') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-05') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-06') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-07') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-08') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-09') and k.tramo = 'Navarro Reverter'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-11-03') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
; 
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-11-04') and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

INSERT INTO trafico_any_keyperformanceindicator (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,diasemana,kpivalue,tramo,"name",measureunitcas,measureunitval) VALUES
('2023-03-28 11:00:00.000+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-03-19','J','804','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.001+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-03-21','S','1661','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.002+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-03-21','S','496','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.003+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-03-22','D','340','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.004+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-03-23','L','565','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.005+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-03-24','M','509','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.006+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-03-24','M','410','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.007+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-03-25','X','685','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.008+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-03-27','V','2329','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.009+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-03-30','L','532','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.010+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-04-01','X','361','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.011+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-04-03','V','338','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.012+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-04-10','V','459','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.013+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-04-11','S','725','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.014+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-04-14','M','662','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.015+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-04-19','D','956','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.016+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-04-20','L','335','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.017+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-04-21','M','373','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.018+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-04-22','X','1085','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.019+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-04-23','J','589','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.020+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-04-30','J','3001','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.021+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-06-22','L','400','Carrer Alacant','Carrer Alacant','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.022+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-06-23','M','47','Carrer Alacant','Carrer Alacant','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.023+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2020-06-24','X','5','Carrer Alacant','Carrer Alacant','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.024+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2021-09-03','V','5150','Pont de les arts','Pont de les arts','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.025+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-04-15','V','23','Carrer Alacant','Carrer Alacant','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.026+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-04-15','V','78','Carrer Russafa','Carrer Russafa','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.027+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-04-16','S','1','Carrer Alacant','Carrer Alacant','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.028+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-04-16','S','2','Carrer Russafa','Carrer Russafa','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.029+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-04-17','D','2','Carrer Alacant','Carrer Alacant','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.030+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-04-17','D','3','Carrer Russafa','Carrer Russafa','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.031+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-05-01','D','44','Navarro Reverter','Navarro Reverter','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.032+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-05-01','D','36','Pont de fusta','Pont de fusta','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.033+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-05-01','D','25','Pont del real','Pont del real','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.034+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-05-04','X','2','Navarro Reverter','Navarro Reverter','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.035+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-05-05','J','1','Navarro Reverter','Navarro Reverter','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.036+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-05-06','V','2','Navarro Reverter','Navarro Reverter','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.037+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-05-07','S','2','Navarro Reverter','Navarro Reverter','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.038+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-05-08','D','2','Navarro Reverter','Navarro Reverter','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.039+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-05-09','L','2','Navarro Reverter','Navarro Reverter','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.040+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-11-03','J','14204','Carrer Russafa','Carrer Russafa','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.041+01','/trafico','Kpi-Trafico-Bicicletas-Accesos-Anillo','KeyPerformanceIndicator','2022-11-04','V','10187','Carrer Russafa','Carrer Russafa','Bicicletas','Bicicletes');

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2021-06-02' and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo') and k.recvtime = '2021-06-03 10:08:27.327';

 
delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2021-09-15' and k.tramo = 'Carrer Russafa'
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo') and k.recvtime = '2021-09-16 06:19:30.294';

delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-03-23', '2021-08-24', '2021-08-25', '2021-08-26') and k.tramo = 'Carrer Colon'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;
delete  from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-04-01') and k.tramo = 'Xàtiva'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

INSERT INTO trafico_any_keyperformanceindicator (recvtime,fiwareservicepath,entityid,entitytype,calculationperiod,diasemana,kpivalue,tramo,"name",measureunitcas,measureunitval) VALUES
('2023-03-28 11:00:00.042+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-03-23','L','840','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.043+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2020-04-01','X','535','Xàtiva','Xàtiva','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.044+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2021-08-24','M','8904','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.045+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2021-08-25','X','13651','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes'),
('2023-03-28 11:00:00.046+01','/trafico','Kpi-Trafico-Bicicletas-Anillo','KeyPerformanceIndicator','2021-08-26','J','6847','Carrer Colon','Carrer Colon','Bicicletas','Bicicletes');


delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2021-06-02' and k.tramo = 'Plaça Tetuan'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo') and k.recvtime = '2021-06-03 00:00:44.465';

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2021-09-15' and k.tramo = 'Plaça Tetuan'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo') and k.recvtime = '2021-09-16 06:19:30.294';
	


COMMIT;
