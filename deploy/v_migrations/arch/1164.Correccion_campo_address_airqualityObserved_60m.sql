-- Deploy gis-sc_vlci:v_migrations/1164.Correccion_campo_address_airqualityObserved_60m to pg

BEGIN;

update sc_vlci.medioambiente_any_airqualityobserved set address  = 'VALÈNCIA CENTRE' where entityid = 'A07_VALENCIACENTRE_60m';

COMMIT;

BEGIN;

update sc_vlci.medioambiente_any_airqualityobserved set address = 'AVDA.FRANCIA', address_md = '[]' where entityid = 'A01_AVFRANCIA_60m' and address is null;
update sc_vlci.medioambiente_any_airqualityobserved set address = 'PATRAIX', address_md = '[]' where entityid = 'A11_PATRAIX_60m' and address is null;
update sc_vlci.medioambiente_any_airqualityobserved set address = 'POLITÈCNIC', address_md = '[]' where entityid = 'A05_POLITECNIC_60m' and address is null;
update sc_vlci.medioambiente_any_airqualityobserved set address = 'OLIVERETA', address_md = '[]' where entityid = 'A10_OLIVERETA_60m' and address is null;
update sc_vlci.medioambiente_any_airqualityobserved set address = 'DR.LLUCH', address_md = '[]' where entityid = 'A08_DR_LLUCH_60m'and address is null;
update sc_vlci.medioambiente_any_airqualityobserved set address = 'PISTA DE SILLA', address_md = '[]' where entityid = 'A04_PISTASILLA_60m'and address is null;
update sc_vlci.medioambiente_any_airqualityobserved set address = 'VALÈNCIA CENTRE', address_md = '[]' where entityid = 'A07_VALENCIACENTRE_60m'and address is null;
update sc_vlci.medioambiente_any_airqualityobserved set address = 'MOLÍ DEL SOL', address_md = '[]' where entityid = 'A03_MOLISOL_60m'and address is null;
update sc_vlci.medioambiente_any_airqualityobserved set address = 'BULEVARD SUD', address_md = '[]' where entityid = 'A02_BULEVARDSUD_60m'and address is null;
update sc_vlci.medioambiente_any_airqualityobserved set address = 'CABANYAL', address_md = '[]' where entityid = 'A09_CABANYAL_60m'and address is null;
update sc_vlci.medioambiente_any_airqualityobserved set address = 'VIVERS', address_md = '[]' where entityid = 'A06_VIVERS_60m'and address is null;

COMMIT;