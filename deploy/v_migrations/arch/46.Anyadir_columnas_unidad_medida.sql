-- Deploy gis-sc_vlci:v_migrations/46.Anyadir_columnas_unidad_medida to pg

BEGIN;

ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator ADD measureunitcas varchar(100) NULL;
ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator ADD measureunitcas_md varchar(100) NULL;
ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator ADD measureunitval varchar(100) NULL;
ALTER TABLE sc_vlci.trafico_any_keyperformanceindicator ADD measureunitval_md varchar(100) NULL;

ALTER TABLE sc_vlci.wifi_any_keyperformanceindicator ADD measureunitcas varchar(100) NULL;
ALTER TABLE sc_vlci.wifi_any_keyperformanceindicator ADD measureunitcas_md varchar(100) NULL;
ALTER TABLE sc_vlci.wifi_any_keyperformanceindicator ADD measureunitval varchar(100) NULL;
ALTER TABLE sc_vlci.wifi_any_keyperformanceindicator ADD measureunitval_md varchar(100) NULL;

ALTER TABLE sc_vlci.emt_any_keyperformanceindicator ADD measureunitcas varchar(100) NULL;
ALTER TABLE sc_vlci.emt_any_keyperformanceindicator ADD measureunitcas_md varchar(100) NULL;
ALTER TABLE sc_vlci.emt_any_keyperformanceindicator ADD measureunitval varchar(100) NULL;
ALTER TABLE sc_vlci.emt_any_keyperformanceindicator ADD measureunitval_md varchar(100) NULL;

ALTER TABLE sc_vlci.medioambiente_any_keyperformanceindicator ADD measurelandunit varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_keyperformanceindicator ADD measurelandunit_md varchar(100) NULL;

ALTER TABLE sc_vlci.aguas_any_keyperformanceindicator ADD measurelandunit varchar(100) NULL;
ALTER TABLE sc_vlci.aguas_any_keyperformanceindicator ADD measurelandunit_md varchar(100) NULL;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD no2type varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD o3type varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD pm10type varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD pm25type varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD so2type varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD no2type_md varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD o3type_md varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD pm10type_md varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD pm25type_md varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD so2type_md varchar(100) NULL;

ALTER TABLE sc_vlci.residuos_any_keyperformanceindicator ADD measureunit varchar(100) NULL;
ALTER TABLE sc_vlci.residuos_any_keyperformanceindicator ADD measureunit_md varchar(100) NULL;


COMMIT;
