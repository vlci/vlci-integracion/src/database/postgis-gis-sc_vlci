-- Deploy gis-sc_vlci:v_migrations/241.anyadir_md_t_datos_cb_trafico_cuenta_bicis_lastdata to pg

BEGIN;


DROP TABLE sc_vlci.t_datos_cb_trafico_cuenta_bicis_lastdata;

CREATE TABLE sc_vlci.t_datos_cb_trafico_cuenta_bicis_lastdata (
    recvtime timestamptz,
    fiwareservicepath varchar(100),
    entityid varchar(100),
    entitytype varchar(100),
    description varchar(200),
    description_md varchar(200),
    descriptionCas varchar(200),
    descriptionCas_md varchar(200),
    dateObserved varchar(100),
    dateObserved_md varchar(200),
    dateObservedFrom varchar(100),
    dateObservedFrom_md varchar(200),
    dateObservedTo varchar(100),
    dateObservedTo_md varchar(200),
    intensity int4,
    intensity_md varchar(200),
    hourlyIntensity int4,
    hourlyIntensity_md varchar(200),
    location public."geometry",
    location_md varchar(200),
    name varchar(200),
    name_md varchar(200),
    operationalStatus varchar(200),
    operationalStatus_md varchar(200),
	CONSTRAINT t_datos_cb_trafico_cuenta_bicis_lastdata_pkey PRIMARY KEY (entityid));

COMMIT;
