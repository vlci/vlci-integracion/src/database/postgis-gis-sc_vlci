-- Deploy gis-sc_vlci:v_migrations/342.Eliminar_col_description_sanitat_kpi to pg

BEGIN;

ALTER TABLE sc_vlci.sanitat_any_keyperformanceindicator DROP COLUMN description;
ALTER TABLE sc_vlci.sanitat_any_keyperformanceindicator DROP COLUMN description_md;

COMMIT;
