-- Deploy gis-sc_vlci:v_migrations/765.Anyadir_contaminantes to pg

BEGIN;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD novalue varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD novalue_md varchar(100) NULL;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD noxvalue varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD noxvalue_md varchar(100) NULL;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD pm1value varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD pm1value_md varchar(100) NULL;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD notype varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD notype_md varchar(100) NULL;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD noxtype varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD noxtype_md varchar(100) NULL;

ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD pm1type varchar(100) NULL;
ALTER TABLE sc_vlci.medioambiente_any_airqualityobserved ADD pm1type_md varchar(100) NULL;

COMMIT;
