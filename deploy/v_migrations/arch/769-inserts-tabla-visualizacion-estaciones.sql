BEGIN;

 INSERT INTO sc_vlci.t_env_medioambiente_visualizacion_estaciones (
            entityid, 
            isVisibleVMinut, 
            postAtGeoportal,
            aud_user_ins,
            aud_user_upd) 
        VALUES 
            ('A01_AVFRANCIA', true, true, 'Ivan-769','Ivan-769'),
            ('A02_BULEVARDSUD', true, true, 'Ivan-769','Ivan-769'),
            ('A03_MOLISOL', true, true, 'Ivan-769','Ivan-769'),
            ('A04_PISTASILLA', true, true, 'Ivan-769','Ivan-769'),
            ('A05_POLITECNIC', true, true, 'Ivan-769','Ivan-769'),
            ('A06_VIVERS', true, true, 'Ivan-769','Ivan-769'),
            ('A07_VALENCIACENTRE', true, true, 'Ivan-769','Ivan-769'),
            ('A08_DR_LLUCH', true, true, 'Ivan-769','Ivan-769'),
            ('A09_CABANYAL', false, false, 'Ivan-769','Ivan-769'),
            ('A10_OLIVERETA', true, true, 'Ivan-769','Ivan-769'),
            ('A11_PATRAIX', true, true, 'Ivan-769','Ivan-769');
END;