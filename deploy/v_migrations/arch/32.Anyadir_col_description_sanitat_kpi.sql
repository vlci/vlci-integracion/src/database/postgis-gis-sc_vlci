-- Deploy gis-sc_vlci:v_migrations/32.Anyadir_col_description_sanitat_kpi to pg

BEGIN;

ALTER TABLE sc_vlci.sanitat_any_keyperformanceindicator ADD description varchar NULL;
ALTER TABLE sc_vlci.sanitat_any_keyperformanceindicator ADD description_md varchar NULL;

COMMIT;
