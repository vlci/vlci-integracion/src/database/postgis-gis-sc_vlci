-- Deploy gis-sc_vlci:v_migrations/1582.cdm_ciutat-borrar-bicis-coches-datos-anomalos to pg

BEGIN;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2017-10-23') and k.tramo = 'Accés a Arxiduc Carles pel Camí nou de Picanya [Entre V-30 i Pedrapiquers]'
  and entityid IN ('Kpi-Accesos-Ciudad-Coches')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod >= '2017-11-23' and k.calculationperiod <= '2018-04-04' and k.tramo = 'Av. de Giorgeta [Entre Sant Vicent i Jesús]'
  and entityid IN ('Kpi-Accesos-Vias-Coches')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2019-12-25'
  and entityid IN ('Kpi-Accesos-Vias-Coches')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2021-11-12'
  and entityid IN ('Kpi-Accesos-Vias-Coches')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2017-09-23' and k.tramo IN ('Av. Blasco Ibáñez [Entre Doctor Moliner i Av. Aragó]', 'Av. Dr. Peset Aleixandre [Entre Joan XXIII i Camí de Moncada]', 'Gran Via de Ferran el Catòlic [Entre Àngel Guimerà i Passeig de la Petxina]', 'Gran Via del Marqués del Túria [Entre Pont d´Aragó i Hernan Cortés]', 'Av. de Peris i Valero [Entre Ausiàs March i Sapadors]')
  and entityid IN ('Kpi-Accesos-Vias-Coches')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2017-09-23' and k.tramo IN ('Av. Blasco Ibáñez [Entre Doctor Moliner i Av. Aragó]', 'Av. Dr. Peset Aleixandre [Entre Joan XXIII i Camí de Moncada]', 'Gran Via de Ferran el Catòlic [Entre Àngel Guimerà i Passeig de la Petxina]', 'Gran Via del Marqués del Túria [Entre Pont d´Aragó i Hernan Cortés]', 'Av. de Peris i Valero [Entre Ausiàs March i Sapadors]')
  and entityid IN ('Kpi-Accesos-Vias-Coches')
;



delete from trafico_any_keyperformanceindicator k
where k.calculationperiod <= '2017-01-26' and k.tramo IN ('Guillem de Castro')
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2017-01-17' and k.tramo IN ('Plaça Tetuan')
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2017-02-17' and k.tramo IN ('Comte de Trénor – Pont de fusta')
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2017-03-21'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2017-04-27'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2017-09-23'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2017-09-24'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod >= '2018-03-27' and k.calculationperiod <= '2018-04-11' and k.tramo IN ('Comte de Trénor – Pont de fusta')
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2018-08-15' and k.tramo IN ('Plaça Tetuan')
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod >= '2018-08-21' and k.calculationperiod <= '2018-08-25' and k.tramo IN ('Plaça Tetuan')
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2019-12-25'
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2021-08-25' and k.tramo IN ('Carrer Colon')
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2022-03-20' and k.tramo IN ('Carrer Colon', 'Comte de Trénor – Pont de fusta', 'Plaça Tetuan')
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod = '2022-05-01' and k.tramo IN ('Carrer Colon', 'Comte de Trénor – Pont de fusta', 'Plaça Tetuan')
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2023-02-04', '2023-02-05') and k.tramo IN ('Carrer Alacant')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-11-03', '2022-11-04') and k.tramo IN ('Carrer Russafa')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-04', '2022-05-05', '2022-05-06', '2022-05-07', '2022-05-08', '2022-05-09') and k.tramo IN ('Navarro Reverter')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-05-01') and k.tramo IN ('Navarro Reverter', 'Pont de fusta', 'Pont del real')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-04-15', '2022-04-16', '2022-04-17') and k.tramo IN ('Carrer Alacant', 'Carrer Russafa')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-03-20') and k.tramo IN ('Pont del real', 'Navarro Reverter', 'Pont de fusta')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2022-02-05', '2022-02-06') and k.tramo IN ('Pont de fusta')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2021-12-11', '2021-12-12') and k.tramo IN ('Navarro Reverter')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2021-11-12') 
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2021-04-18') and k.tramo IN ('Pont de fusta')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;


delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-11-13', '2020-11-14', '2020-11-15', '2020-11-16', '2020-11-17', '2020-11-23', '2020-09-30', '2020-04-30', '2019-10-08', '2019-09-21', '2019-09-22') and k.tramo IN ('Pont de fusta')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

delete from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-06-22', '2020-06-23', '2020-06-24', '2019-09-14', '2019-09-15') and k.tramo IN ('Carrer Alacant')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo')
;

COMMIT;
