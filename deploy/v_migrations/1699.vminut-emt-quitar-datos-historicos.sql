-- Deploy gis-sc_vlci:v_migrations/1699.vminut-emt-quitar-datos-historicos to pg

BEGIN;

    delete from 
      sc_vlci.emt_any_keyperformanceindicator
    where
      extract(year from calculationperiod) < 2020;

COMMIT;
