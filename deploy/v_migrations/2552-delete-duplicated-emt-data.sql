-- Deploy gis-sc_vlci:v_migrations/2552-delete-duplicated-emt-data to pg

BEGIN;

delete from sc_vlci.emt_any_keyperformanceindicator where calculationperiod = '2024-02-24' and recvtime < '2024-02-28';
delete from sc_vlci.emt_any_keyperformanceindicator where calculationperiod = '2024-02-23' and recvtime < '2024-02-27 13:00:00.000';

COMMIT;
