-- Deploy gis-sc_vlci:v_migrations/3138.eliminar_tablas_prefijo_eliminar to pg

BEGIN;

 DROP TABLE IF EXISTS sc_vlci.eliminar_medioambiente_any_airqualityobserved,
                      sc_vlci.eliminar_medioambiente_emt_any_keyperformanceindicator,
                      sc_vlci.eliminar_sanitat_any_keyperformanceindicator,
                      sc_vlci.eliminar_trafico_any_offstreetparking;

COMMIT;
