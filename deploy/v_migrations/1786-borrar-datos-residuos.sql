-- Deploy gis-sc_vlci:v_migrations/1786-borrar-datos-residuos to pg

BEGIN;
delete from
sc_vlci.residuos_any_keyperformanceindicator
where to_date(calculationperiod,'dd-mm-yyyy') >= '01-03-2020'
and to_date(calculationperiod,'dd-mm-yyyy') < '01-01-2022';

COMMIT;
