-- Deploy gis-sc_vlci:v_migrations/1762-borrado-residuos-enero2023 to pg

BEGIN;

delete from sc_vlci.residuos_any_keyperformanceindicator
where to_date(calculationperiod,'dd-mm-yyyy') >= to_date('01-01-2023','dd-mm-yyyy')
and to_date(calculationperiod,'dd-mm-yyyy') <= to_date('31-01-2023','dd-mm-yyyy');

COMMIT;
