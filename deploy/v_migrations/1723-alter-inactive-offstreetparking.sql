-- Deploy gis-sc_vlci:v_migrations/1723-alter-inactive-offstreetparking to pg

BEGIN;

ALTER TABLE trafico_any_offstreetparking RENAME COLUMN  operationalstatusnodataIgnore TO inactive;
ALTER TABLE trafico_any_offstreetparking RENAME COLUMN  operationalstatusnodataIgnore_md TO inactive_md;

COMMIT;
