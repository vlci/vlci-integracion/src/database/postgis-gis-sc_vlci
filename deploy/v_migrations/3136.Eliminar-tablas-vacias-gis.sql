-- Deploy gis-sc_vlci:v_migrations/3136.Eliminar-tablas-vacias-gis to pg

BEGIN;

DROP TABLE IF EXISTS 
    sc_vlci.medioambiente_any_weatherobserved,
    sc_vlci.mercadoruzafa_aforo_casetasegur_2575_e3t_ls40,
    sc_vlci.mercadoruzafa_aforo_dentisana_2584_e3t_ls40,
    sc_vlci.mercadoruzafa_aforo_entradaipetrona_2580_e3t_ls40,
    sc_vlci.mercadoruzafa_aforo_grupo90_2578_e3t_ls40,
    sc_vlci.mercadoruzafa_aforo_improntas_pescaext_2579_e3t_ls40,
    sc_vlci.mercadoruzafa_aforo_mrgold_2583_e3t_ls40,
    sc_vlci.mercadoruzafa_aforo_rosador_2585_e3t_ls40,
    sc_vlci.mercadoruzafa_aforo_trixka_2581_e3t_ls40,
    sc_vlci.mercadoruzafa_consumo_consumoaireacont_2552_e3t_lb40,
    sc_vlci.mercadoruzafa_consumo_consumogeneral_2551_e3t_lb40,
    sc_vlci.mercadoruzafa_disuasorio_entradaipetrona_2574_e3t_ls40,
    sc_vlci.mercadoruzafa_disuasorio_grupo90_2576_e3t_ls40,
    sc_vlci.mercadoruzafa_disuasorio_improntas_pescaext_2586_e3t_ls40,
    sc_vlci.mercadoruzafa_disuasorio_mrgold_2573_e3t_ls40,
    sc_vlci.mercadoruzafa_ruido_2550_e3t_ls40,
    sc_vlci.mercadoruzafa_sensaciontermica_2553_e3t_ls40,
    sc_vlci.mercadoruzafa_sensaciontermica_2554_e3t_ls40,
    sc_vlci.mercadoruzafa_sensaciontermica_2555_e3t_ls40,
    sc_vlci.mercadoruzafa_sensaciontermica_2556_e3t_ls40,
    sc_vlci.mercadoruzafa_sensaciontermica_temperaturapanel_2557_e3t_ls40,
    sc_vlci.mercadoruzafa_sensaciontermica_temppescaderia_2558_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_avdaalfauir_2604_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_bomberoscecom_2608_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_bomberossaler_2634_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_callealta_2609_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_gulliver_2633_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_laboratorioavfranc_2610_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_molidelsol_2601_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_naturia_2607_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_parquebombcampanar_2605_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_playamalva_2623_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_policiaalfara_2635_e3t_ls40,
    sc_vlci.radiacion_laboratoriomunicipal_policiamaritimo_2606_e3t_ls40,
    sc_vlci.residuos_alcaldia_any_keyperformanceindicator,
    sc_vlci.sc_vlci_error_log;

COMMIT;
