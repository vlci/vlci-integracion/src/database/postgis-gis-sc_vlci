-- Deploy gis-sc_vlci:v_migrations/2305.delete-isVisibleVMinut to pg

BEGIN;

ALTER TABLE sc_vlci.t_env_medioambiente_visualizacion_estaciones
DROP COLUMN isVisibleVMinut;

COMMIT;
