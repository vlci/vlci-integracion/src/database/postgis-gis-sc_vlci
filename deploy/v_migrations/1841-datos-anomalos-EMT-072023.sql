-- Deploy gis-sc_vlci:v_migrations/1841-datos-anomalos-EMT-072023 to pg

BEGIN;

DELETE FROM  sc_vlci.emt_any_keyperformanceindicator where calculationperiod in ('2023-07-03','2023-07-28','2023-07-29');

COMMIT;
