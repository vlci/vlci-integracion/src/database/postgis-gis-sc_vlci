-- Deploy gis-sc_vlci:v_migrations/3485.eliminar_flujo_datos_vlci_gis_road to pg

BEGIN;

  DROP TABLE IF EXISTS sc_vlci.trafico_any_road;

  DROP FUNCTION IF EXISTS sc_vlci.update_estadotrafico_realtime;
  
COMMIT;

