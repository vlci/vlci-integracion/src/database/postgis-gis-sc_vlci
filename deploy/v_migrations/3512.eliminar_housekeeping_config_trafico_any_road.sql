-- Deploy gis-sc_vlci:v_migrations/3512.eliminar_housekeeping_config_trafico_any_road to pg
BEGIN;

DELETE FROM sc_vlci.housekeeping_config
where
  table_nam = 'trafico_any_road'
  and tiempo = 1
  and colfecha = 'recvtime';

COMMIT;