-- Deploy gis-sc_vlci:r_code/functions/tg.t_env_medioambiente_visualizacion_estaciones_update to pg

BEGIN;

create trigger t_env_medioambiente_visualizacion_estaciones_before_update before
insert on sc_vlci.t_env_medioambiente_visualizacion_estaciones 
for each row execute procedure sc_vlci.fn_current_timestamp_before_update();

COMMIT;
