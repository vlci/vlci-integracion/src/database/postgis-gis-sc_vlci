-- Deploy gis-sc_vlci:r_code/functions/f.fn_aud_fec_upd to pg

BEGIN;

	CREATE OR REPLACE FUNCTION sc_vlci.fn_aud_fec_upd()
	RETURNS trigger
	LANGUAGE plpgsql
	AS $function$
	BEGIN
		NEW.aud_fec_upd = CURRENT_TIMESTAMP;

		if NEW.aud_user_upd is null  then 
			NEW.aud_user_upd = 'CYGNUS';
		end if;

		RETURN NEW;
	END;
	$function$
	;

COMMIT;
