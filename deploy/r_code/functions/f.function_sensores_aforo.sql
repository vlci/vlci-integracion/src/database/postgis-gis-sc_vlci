-- Deploy gis-sc_vlci:r_code/functions/f.function_sensores_aforo to pg

BEGIN;

CREATE OR REPLACE FUNCTION sc_vlci.function_sensores_aforo()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE v_oldvalue int;
DECLARE v_oldvalue_entradas int;
DECLARE v_oldvalue_salidas int;
DECLARE v_oldvalue_acumulado int;
DECLARE v_newvalue int;
DECLARE v_id int;
DECLARE v_suma int;
DECLARE v_entradas int;
DECLARE v_salidas int;
DECLARE v_acumulados int;
-- DECLARE v_total int;
BEGIN
    v_id := (select right(new.entityid,position('_' in reverse(new.entityid))-1)::int);
	v_oldvalue := (select contador from gis.sensores_aforo where id = v_id);
    v_oldvalue_entradas := (select entradas from gis.sensores_aforo where id = v_id);
    v_oldvalue_salidas := (select salidas from gis.sensores_aforo where id = v_id);
    v_oldvalue_acumulado := (select acumulado from gis.sensores_aforo where id = v_id);
	v_newvalue := new.counter::int;
	v_suma := v_oldvalue + v_newvalue;

    IF v_newvalue > 0 THEN
        v_entradas := v_oldvalue_entradas + v_newvalue;
        v_acumulados := v_oldvalue_salidas + v_entradas;

        UPDATE gis.sensores_aforo
        SET entradas = v_entradas
        WHERE id = v_id;
    ELSIF v_newvalue < 0 THEN
        v_salidas := v_oldvalue_salidas + (v_newvalue * -1);
        v_acumulados := v_oldvalue_entradas + v_salidas;

        UPDATE gis.sensores_aforo 
        SET salidas = v_salidas
        WHERE id = v_id;
    ELSE
        v_acumulados := v_oldvalue_acumulado;
    END IF;

    UPDATE gis.sensores_aforo 
    SET contador = v_suma, acumulado = v_acumulados
    WHERE id = v_id;

    -- v_total := (select sum(contador) from gis.sensores_aforo where nombre != 'Aforo_Total');

    -- UPDATE gis.sensores_aforo 
    -- SET contador = v_total
    -- WHERE nombre = 'Aforo_Total';

           RETURN new;
END;
$function$
;


COMMIT;
