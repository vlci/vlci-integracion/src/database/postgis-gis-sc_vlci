-- Deploy gis-sc_vlci:r_code/functions/f.update_estatautomaticas_contaminantes to pg

BEGIN;

CREATE OR REPLACE FUNCTION sc_vlci.update_estatautomaticas_contaminantes()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN

IF NEW.entityid like ('%_60m') and NEW.entityid not in (select concat(entityid,'_60m') from t_env_medioambiente_visualizacion_estaciones
where postatgeoportal is false) THEN

   --Excepcion para que no aparezcan los valores de los contaminantes pm10 y pm25 en la estacion del cabanyal.
   IF NEW.gis_id ='Cabanyal' THEN 
   NEW.PM10Value = '';
   NEW.PM25Value = '';
   END IF;


   UPDATE gis.estatautomaticas_evw SET 
   so2= CASE WHEN NEW.SO2Value='' THEN NULL ELSE cast(NEW.SO2Value AS NUMERIC(4,1)) END,
   no2= CASE WHEN NEW.NO2Value='' THEN NULL ELSE cast(NEW.NO2Value AS NUMERIC(4,1)) END,
   o3= CASE WHEN NEW.O3Value='' THEN NULL ELSE cast(NEW.O3Value AS NUMERIC(4,1)) END,
   pm10= CASE WHEN NEW.PM10Value='' THEN NULL ELSE cast(NEW.PM10Value AS NUMERIC(4,1)) END,
   pm25= CASE WHEN NEW.PM25Value='' THEN NULL ELSE cast(NEW.PM25Value AS NUMERIC(4,1)) END,
   calidad_ambiental= NEW.calidad_ambiental,
   fecha_carga=NEW.recvTime 
   WHERE nombre=NEW.gis_id;
   
   IF NEW.SO2Value='' THEN NEW.SO2Value=NULL; NEW.SO2Value_md=NULL; END IF;
   IF NEW.NO2Value='' THEN NEW.NO2Value=NULL; NEW.NO2Value_md=NULL; END IF;
   IF NEW.O3Value='' THEN NEW.O3Value=NULL; NEW.O3Value_md=NULL; END IF;
   IF NEW.PM10Value='' THEN NEW.PM10Value=NULL; NEW.PM10Value_md=NULL; END IF;
   IF NEW.PM25Value='' THEN NEW.PM25Value=NULL; NEW.PM25Value_md=NULL; END IF;
END IF;
   
   RETURN NEW;
END;
$function$
;
COMMIT;
