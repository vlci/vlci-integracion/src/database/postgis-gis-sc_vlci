-- Deploy gis-sc_vlci:r_code/functions/f.update_intensidad_espiras_realtime to pg

BEGIN;

CREATE OR REPLACE FUNCTION sc_vlci.update_intensidad_espiras_realtime()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
    IF new.entityId LIKE 'tramo_%' THEN
   
		UPDATE gis.tra_intensidad_trafico_evw 
        SET lectura= cast(NEW.intensity AS INTEGER),
        tipo_vehiculo = NEW.typedata
		WHERE idtramo=NEW.name;
		RETURN NEW;
	
	END IF;
   
    IF new.entityId LIKE 'puntoMedida_%' THEN
	
		IF new.typeData = 'TRAFICO' THEN
   
			UPDATE gis.tra_espiras_p_evw SET 
			fecha_actualizacion= cast(NEW.dateObserved AS DATE),
			hora_actualizacion= cast(NEW.dateObserved AS TIMESTAMP without time zone),
			ih= cast(NEW.intensity AS INTEGER)
			WHERE idpm=cast(NEW.laneID AS INTEGER);
		
		END IF;
		
		IF new.typeData = 'BICI' THEN
		
			UPDATE gis.tra_espiras_bici_p_evw SET 
			fecha_actualizacion= cast(NEW.dateObserved AS DATE),
			hora_actualizacion= cast(NEW.dateObserved AS TIMESTAMP without time zone),
			ih= cast(NEW.intensity AS INTEGER)
			WHERE idpm=cast(NEW.laneID AS INTEGER);
		
		END IF;
		
		RETURN NEW;
   
    END IF;
  
END;
$function$
;



COMMIT;
