-- Deploy gis-sc_vlci:r_code/functions/f.function_sensores_disuasorio to pg

BEGIN;

CREATE OR REPLACE FUNCTION sc_vlci.function_sensores_disuasorio()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE v_id int;
BEGIN
    v_id := (select right(new.entityid,position('_' in reverse(new.entityid))-1)::int);

    UPDATE gis.sensores_disuasorio
    SET presencia = new.presence::int
    WHERE id = v_id;

           RETURN new;
END;
$function$
;


COMMIT;
