-- Deploy gis-sc_vlci:r_code/functions/f.current_timestamp_before_update to pg

BEGIN;

-- Esta funcion es generica por lo que puede usarse en todos los triggers 
-- que tengan por objetivo insertar la fecha de auditoria en el momento de actualización de una fila.

CREATE OR REPLACE FUNCTION sc_vlci.fn_current_timestamp_before_update()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
	NEW.aud_fec_upd=CURRENT_TIMESTAMP;
    RETURN NEW;
END;
$function$
;

COMMIT;
