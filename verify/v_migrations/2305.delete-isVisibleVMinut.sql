-- Verify gis-sc_vlci:v_migrations/2305.delete-isVisibleVMinut on pg

BEGIN;

SELECT 1 / (case count(*) when 0 then 1 else 0 end)
FROM information_schema.columns
WHERE table_name = 't_env_medioambiente_visualizacion_estaciones'
AND column_name = 'isvisiblevminut';

ROLLBACK;
