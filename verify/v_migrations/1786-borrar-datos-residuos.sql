-- Verify gis-sc_vlci:v_migrations/1786-borrar-datos-residuos on pg

BEGIN;

select case when count(*)=0 then 1 else 1/(select 0) end as resultado
from sc_vlci.residuos_any_keyperformanceindicator
where to_date(calculationperiod,'dd-mm-yyyy') >= '01-03-2020'
and to_date(calculationperiod,'dd-mm-yyyy') < '01-01-2022';


select case when count(*)=10392 then 1 else 1/(select 0) end as resultado
from sc_vlci.residuos_any_keyperformanceindicator 
where to_date(calculationperiod,'dd-mm-yyyy') >= '01-01-2022'
and to_date(calculationperiod,'dd-mm-yyyy') < '01-06-2023';

ROLLBACK;
