-- Verify gis-sc_vlci:v_migrations/2425.corregir-datos-anomalos-emt on pg

BEGIN;

SELECT 1 / CASE WHEN total = 8045140 THEN 1 ELSE 0 END
FROM (
     SELECT sum(kpivalue) as total
     FROM sc_vlci.emt_any_keyperformanceindicator emt
     WHERE calculationperiod IN (
          '2023-12-06',
          '2023-10-29',
          '2023-10-28',
          '2023-10-13',
          '2023-09-20',
          '2023-09-13',
          '2023-09-08',
          '2023-09-07',
          '2023-09-02',
          '2023-08-31',
          '2023-08-28',
          '2023-08-25',
          '2023-08-22',
          '2023-08-21',
          '2023-08-20',
          '2023-08-19',
          '2023-08-18')
          ) as total_viajeros;

SELECT 1 / (CASE WHEN COUNT(*) = 0 THEN 1 ELSE 0 END) FROM sc_vlci.emt_any_keyperformanceindicator
WHERE calculationperiod IN ('2023-11-29', '2023-12-11', '2023-10-30', '2023-11-03');

ROLLBACK;
