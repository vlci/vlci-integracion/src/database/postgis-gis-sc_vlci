-- Verify gis-sc_vlci:v_migrations/3483.eliminar_flujo_datos_vlci_gis_offstreetparking on pg
BEGIN;

SELECT
  1 / (
    CASE count(*)
      WHEN 0 THEN 1
      ELSE 0
    END
  )
FROM
  pg_proc
WHERE
  proname = 'update_aparcamientos_realtime';

ROLLBACK;