-- Verify gis-sc_vlci:v_migrations/1841-datos-anomalos-EMT-072023 on pg

BEGIN;

select 1/ (case when count(*) = 0 then 1 else 0 end)  from sc_vlci.emt_any_keyperformanceindicator
where calculationperiod = '2023-07-03';

select 1/ (case when count(*) = 0 then 1 else 0 end)  from sc_vlci.emt_any_keyperformanceindicator
where calculationperiod = '2023-07-28';

select 1/ (case when count(*) = 0 then 1 else 0 end)  from sc_vlci.emt_any_keyperformanceindicator
where calculationperiod = '2023-07-29';

ROLLBACK;
