-- Verify gis-sc_vlci:v_migrations/1759-corregir-datos-anomalos-emt on pg

BEGIN;

select 1/ (case when count(*) = 0 then 1 else 0 end)  from sc_vlci.emt_any_keyperformanceindicator
    where calculationperiod in ('2023-06-09', '2023-06-10', '2023-06-11') and sliceanddice1 in('Titulo', 'Ruta');

select 1/(case when count(*) >= 107195 then 1 else 0 end) from sc_vlci.emt_any_keyperformanceindicator

ROLLBACK;
