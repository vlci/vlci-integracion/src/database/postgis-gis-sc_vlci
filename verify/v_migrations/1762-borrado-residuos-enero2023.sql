-- Verify gis-sc_vlci:v_migrations/1762-borrado-residuos-enero2023 on pg

BEGIN;

select case when count(*)=0 then 1 else 1/(select 0) end as resultado
from sc_vlci.residuos_any_keyperformanceindicator 
where to_date(calculationperiod,'dd-mm-yyyy') >= to_date('01-01-2023','dd-mm-yyyy')
and to_date(calculationperiod,'dd-mm-yyyy') <= to_date('31-01-2023','dd-mm-yyyy');

select case when count(*)=13612 then 1 else 1/(select 0) end as resultado
from sc_vlci.residuos_any_keyperformanceindicator 
where to_date(calculationperiod,'dd-mm-yyyy') >= to_date('01-01-2022','dd-mm-yyyy')
and to_date(calculationperiod,'dd-mm-yyyy') <= to_date('31-05-2023','dd-mm-yyyy');

ROLLBACK;
