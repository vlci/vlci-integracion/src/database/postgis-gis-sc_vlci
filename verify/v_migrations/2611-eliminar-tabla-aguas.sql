-- Verify gis-sc_vlci:v_migrations/2611-eliminar-tabla-aguas on pg

BEGIN;

SELECT 1 / (case count(*) when 0 then 1 else 0 end) FROM information_schema.tables WHERE table_name = 'sc_vlci.aguas_any_keyperformanceindicator';

ROLLBACK;
