-- Verify gis-sc_vlci:v_migrations/3481-eliminar-flujo-datos-vlci-gis-airqualityobserved on pg

BEGIN;

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_proc where proname = 'update_estatautomaticas_contaminantes';

ROLLBACK;
