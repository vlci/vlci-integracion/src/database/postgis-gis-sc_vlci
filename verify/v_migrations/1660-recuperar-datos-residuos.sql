-- Verify gis-sc_vlci:v_migrations/1660-recuperar-datos-residuos on pg

BEGIN;

select case when count(*) >= 13492 then 1 else 1/(select 0) end as resultado  
from sc_vlci.residuos_any_keyperformanceindicator
where TO_DATE(calculationperiod ,'DD-MM-YYYY') >= '2022-01-01';

ROLLBACK;
