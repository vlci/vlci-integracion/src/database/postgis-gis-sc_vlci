-- Verify gis-sc_vlci:v_migrations/3480.eliminar_modificar_tablas_funciones_no_utilizadas on pg

BEGIN;

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_proc where proname = 'function_sensores_aforo';

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_proc where proname = 'function_sensores_consumo';

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_proc where proname = 'function_sensores_disuasorio';

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_proc where proname = 'function_sensores_radiacion';

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_proc where proname = 'function_sensores_ruido';

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_proc where proname = 'function_sensores_sensacion_termica';

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_proc where proname = 'update_sanitat_inserttimestamp';


SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_catalog.pg_tables
WHERE schemaname = 'sc_vlci'
AND tablename = 't_env_medioambiente_visualizacion_estaciones';

SELECT 1/(case count(*) when 0 then 0 else 1 end)
FROM pg_catalog.pg_tables
WHERE schemaname = 'sc_vlci'
AND tablename = 'eliminar_t_env_medioambiente_visualizacion_estaciones';



ROLLBACK;
