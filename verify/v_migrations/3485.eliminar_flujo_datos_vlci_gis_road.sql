-- Verify gis-sc_vlci:v_migrations/3485.eliminar_flujo_datos_vlci_gis_road on pg

BEGIN;

  SELECT 1/(case count(*) when 0 then 1 else 0 end)
  FROM pg_proc where proname = 'update_estadotrafico_realtime';



  SELECT 1/(case count(*) when 0 then 1 else 0 end)
  FROM pg_catalog.pg_tables
  WHERE schemaname = 'sc_vlci'
  AND tablename = 'trafico_any_road';

  SELECT 1/(case count(*) when 0 then 1 else 0 end)
  FROM information_schema.triggers
  where trigger_schema = 'sc_vlci'
  and trigger_name = 'insert_road_to_estadotrafico';

ROLLBACK;

