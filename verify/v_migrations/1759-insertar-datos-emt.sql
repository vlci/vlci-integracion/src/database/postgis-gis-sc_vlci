-- Verify gis-sc_vlci:v_migrations/1759-insertar-datos-emt on pg

BEGIN;

select 1/case when count(*) = 241 then 1 else 0 end from sc_vlci.emt_any_keyperformanceindicator
    where sliceanddice1 in('Titulo', 'Ruta') and
        calculationperiod in ('2023-06-09', '2023-06-10', '2023-06-11');

select 1/case when count(*) >= 107195 then 1 else 0 end from sc_vlci.emt_any_keyperformanceindicator;

ROLLBACK;
