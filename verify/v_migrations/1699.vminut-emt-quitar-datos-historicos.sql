-- Verify gis-sc_vlci:v_migrations/1699.vminut-emt-quitar-datos-historicos on pg

BEGIN;

    select 
      1/(
        case 
            when count(*) = 0 then 1
            else 0
        end
      )
    from 
      sc_vlci.emt_any_keyperformanceindicator
    where
      extract(year from calculationperiod) < 2020;

ROLLBACK;
