-- Verify gis-sc_vlci:v_migrations/3512.eliminar_housekeeping_config_trafico_any_road on pg
BEGIN;

select
  1 / (
    case
      when count(*) = 0 then 1
      else 0
    end
  )
from
  sc_vlci.housekeeping_config
WHERE
  table_nam = 'trafico_any_road'
  AND tiempo = 1
  AND colfecha = 'recvtime';

ROLLBACK;