-- Verify gis-sc_vlci:v_migrations/2325.eliminar_flujo_datos_vlci_gis_estatmosfericas on pg
BEGIN;

SELECT
  1 / (
    case count(*)
      when 0 then 1
      else 0
    end
  )
FROM
  pg_proc
where
  proname = 'update_estatatmosfericas_medidas';

ROLLBACK;