-- Verify gis-sc_vlci:v_migrations/2536-datos-anomalos-emt on pg

BEGIN;

SELECT 1 / CASE WHEN total = 5051552 THEN 1 ELSE 0 END
FROM (
     SELECT sum(kpivalue) as total
     FROM sc_vlci.emt_any_keyperformanceindicator emt
     WHERE calculationperiod IN (
        '2024-02-14',
        '2024-02-02',
        '2024-01-24',
        '2023-06-18',
        '2022-12-06',
        '2022-12-04',
        '2022-09-25',
        '2022-09-24',
        '2022-09-19',
        '2022-09-18',
        '2022-03-06',
        '2022-02-06',
        '2022-01-30',
        '2022-01-23',
        '2022-01-09')
        ) as total_viajeros;

ROLLBACK;
