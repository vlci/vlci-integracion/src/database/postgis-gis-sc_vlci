-- Verify gis-sc_vlci:v_migrations/1714-eliminar-datos-anomalos-emt on pg

BEGIN;

select 1/(case when count(*) = 0 then 1 else 0 end) from
	sc_vlci.emt_any_keyperformanceindicator
where
	sliceanddice1 in('Titulo', 'Ruta')
	and calculationperiod in ('2023/05/28', '2023/05/23', '2023/05/21', '2023/05/20', '2023/05/19'); 

select 1/(case when count(*) >= 42650 then 1 else 0 end) from sc_vlci.emt_any_keyperformanceindicator;

ROLLBACK;
