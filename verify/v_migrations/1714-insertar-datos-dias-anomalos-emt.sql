-- Verify gis-sc_vlci:v_migrations/1714-insertar-datos-dias-anomalos-emt on pg

BEGIN;

select 1/case when count(*) > 0 then 1 else 0 end from sc_vlci.emt_any_keyperformanceindicator   eak
where
	sliceanddice1 in('Titulo', 'Ruta')
	and calculationperiod in ('2023/05/28', '2023/05/23', '2023/05/21', '2023/05/20', '2023/05/19'); 

SELECT 1 / CASE WHEN COUNT(*) > 105407 THEN 1 ELSE 0 END
FROM sc_vlci.emt_any_keyperformanceindicator ;

ROLLBACK;
