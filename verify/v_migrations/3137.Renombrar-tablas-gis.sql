-- Verify gis-sc_vlci:v_migrations/3137.Renombrar-tablas-gis on pg

BEGIN;

SELECT 1/ (case when COUNT(*) = 4 then 1 else 0 end) FROM pg_tables 
WHERE schemaname = 'sc_vlci'
and tablename like 'eliminar_%';

ROLLBACK;
