-- Verify gis-sc_vlci:v_migrations/1568-datos-historicos-bsg on pg

BEGIN;

-- Comprueba que se hayan insertado el numero de filas esperado.
select case when count(*)=3607 then 1 else 1/(select 0) end as resultado  
from sc_vlci.medioambiente_any_airqualityobserved
where dateobserved < '2021-06-03 00:00:00';

ROLLBACK;
