-- Verify gis-sc_vlci:v_migrations/1562-insert-kpi-trafico-bicicletas-anillo on pg

BEGIN;

select case when count(*)=1660 then 1 else 1/(select 0) end as resultado  
from sc_vlci.trafico_any_keyperformanceindicator where calculationperiod < to_date('2020-11-28', 'YYYY-MM-DD') 
    and entityid = 'Kpi-Trafico-Bicicletas-Anillo';

ROLLBACK;
