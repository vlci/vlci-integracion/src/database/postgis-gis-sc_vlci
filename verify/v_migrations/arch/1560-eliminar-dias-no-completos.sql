-- Verify gis-sc_vlci:v_migrations/1560-eliminar-dias-no-completos on pg

BEGIN;

select 1/(60-count(*)) from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2021-02-01', '2020-05-31', '2020-06-01', '2020-12-02', '2021-03-06', '2021-05-02', '2021-06-21', '2021-10-31', '2021-11-01', '2021-11-13', '2021-11-14', '2022-02-22', '2022-03-30', '2022-04-10', '2022-06-05', '2023-02-13')
  and entityid IN ('Kpi-Accesos-Ciudad-Coches', 'Kpi-Accesos-Vias-Coches');

ROLLBACK;
