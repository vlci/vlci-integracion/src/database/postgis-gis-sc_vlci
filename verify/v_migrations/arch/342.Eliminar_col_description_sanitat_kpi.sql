-- Verify gis-sc_vlci:v_migrations/342.Eliminar_col_description_sanitat_kpi on pg

BEGIN;

select 1 /
	(case count(*)
   when 0 then 1
	else 0
end)
  FROM information_schema.columns
 WHERE table_schema = 'sc_vlci'
   AND table_name   = 'sanitat_any_keyperformanceindicator'
   and column_name  in ('description','description_md');

ROLLBACK;
