-- Verify gis-sc_vlci:v_migrations/1519.delete_tabla_widget_bicis on pg

BEGIN;

SELECT 1 / (case count(*) when 0 then 1 else 0 end) FROM information_schema.tables WHERE table_name = 'sc_vlci.t_datos_cb_trafico_cuenta_bicis_lastdata';

ROLLBACK;
