-- Verify gis-sc_vlci:v_migrations/847.nameShort-trafico-kpi on pg

BEGIN;

select nameshort,nameshort_md from sc_vlci.trafico_any_keyperformanceindicator where nameshort is not null;

ROLLBACK;
