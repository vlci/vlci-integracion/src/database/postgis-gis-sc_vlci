-- Verify gis-sc_vlci:v_migrations/1581.recuperar-datos-faltantes-bicis on pg

BEGIN;

select 1/(9-count(*)) from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-05-31','2020-06-01','2020-12-02','2021-02-01','2021-03-27','2021-06-21','2021-07-05','2021-10-31','2021-11-01','2021-11-13','2021-11-14','2022-03-09','2022-03-30','2023-02-13')
  and entityid IN ('Kpi-Trafico-Bicicletas-Accesos-Anillo');

select 1/(8-count(*)) from trafico_any_keyperformanceindicator k
where k.calculationperiod IN ('2020-05-31','2020-06-01','2020-12-02','2021-02-01','2021-03-27','2021-06-21','2021-07-05','2021-10-31','2021-11-01','2021-11-13','2021-11-14','2022-03-09','2022-03-30','2023-02-13')
  and entityid IN ('Kpi-Trafico-Bicicletas-Anillo');

ROLLBACK;
