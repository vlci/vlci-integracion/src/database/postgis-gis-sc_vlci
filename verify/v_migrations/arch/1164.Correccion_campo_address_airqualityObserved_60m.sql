-- Verify gis-sc_vlci:v_migrations/1164.Correccion_campo_address_airqualityObserved_60m on pg

BEGIN;

select 1/count(*) from sc_vlci.medioambiente_any_airqualityobserved where address = 'VALÈNCIA CENTRE';

ROLLBACK;


BEGIN;

select 1/(case count(*) when 0 then 1 else 0 end)from sc_vlci.medioambiente_any_airqualityobserved where entityid in ('A01_AVFRANCIA_60m','A11_PATRAIX_60m', 'A05_POLITECNIC_60m','A10_OLIVERETA_60m', 'A08_DR_LLUCH_60m',
	'A04_PISTASILLA_60m','A07_VALENCIACENTRE_60m', 'A03_MOLISOL_60m', 'A02_BULEVARDSUD_60m', 'A02_BULEVARDSUD_60m', 'A09_CABANYAL_60m', 'A06_VIVERS_60m') and address is null;
ROLLBACK;