-- Verify gis-sc_vlci:v_migrations/46.Anyadir_columnas_unidad_medida on pg

BEGIN;

select measureunitcas, measureunitcas_md, measureunitval, measureunitval_md from sc_vlci.trafico_any_keyperformanceindicator; 
select measureunitcas, measureunitcas_md, measureunitval, measureunitval_md from sc_vlci.wifi_any_keyperformanceindicator;
select measureunitcas, measureunitcas_md, measureunitval, measureunitval_md from sc_vlci.emt_any_keyperformanceindicator;
select measurelandunit, measurelandunit_md from  sc_vlci.medioambiente_any_keyperformanceindicator;
select measurelandunit, measurelandunit_md from sc_vlci.aguas_any_keyperformanceindicator;
select no2type, o3type, pm10type, pm25type, so2type, no2type_md, o3type_md, pm10type_md, pm25type_md, so2type_md from sc_vlci.medioambiente_any_airqualityobserved;
select measureunit, measureunit_md from sc_vlci.residuos_any_keyperformanceindicator;

ROLLBACK;
