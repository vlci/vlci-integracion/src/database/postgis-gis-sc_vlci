-- Verify gis-sc_vlci:v_migrations/241.anyadir_md_t_datos_cb_trafico_cuenta_bicis_lastdata on pg

BEGIN;

SELECT *
  FROM sc_vlci.t_datos_cb_trafico_cuenta_bicis_lastdata
 WHERE FALSE;

ROLLBACK;
