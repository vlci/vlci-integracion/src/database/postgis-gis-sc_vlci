-- Verify gis-sc_vlci:v_migrations/1559.cmdciutat-residuos-corregir-dato-incorrecto on pg

BEGIN;

select 1/count(*) from sc_vlci.residuos_any_keyperformanceindicator where calculationperiod = '04-01-2023' and sliceanddicevalue1  = 'Zona 2' and sliceanddicevalue2 = 'Orgànica' and kpivalue = '63460';
select 1/count(*) from sc_vlci.residuos_any_keyperformanceindicator where calculationperiod = '04-01-2023' and sliceanddicevalue1  = 'Total' and sliceanddicevalue2 = 'Orgànica' and kpivalue = '132380';

ROLLBACK;
