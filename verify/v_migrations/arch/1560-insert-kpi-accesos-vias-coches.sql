-- Verify gis-sc_vlci:v_migrations/1560-insert-kpi-accesos-vias-coches on pg

BEGIN;

select case when count(*)=1992 then 1 else 1/(select 0) end as resultado  
from sc_vlci.trafico_any_keyperformanceindicator where calculationperiod < to_date('2020-11-28', 'YYYY-MM-DD') 
    and entityid = 'Kpi-Accesos-Vias-Coches';

ROLLBACK;
