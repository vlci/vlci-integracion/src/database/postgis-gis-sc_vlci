-- Verify gis-sc_vlci:v_migrations/1567.historicos_EMT_mob006-04_2018 on pg

BEGIN;

select case when count(*)=20271 then 1 else 1/(select 0) end as resultado  
from sc_vlci.emt_any_keyperformanceindicator
where entityid = 'mob006-04'
and calculationperiod < '2019-01-01'
and calculationperiod >= '2018-01-01';

ROLLBACK;
