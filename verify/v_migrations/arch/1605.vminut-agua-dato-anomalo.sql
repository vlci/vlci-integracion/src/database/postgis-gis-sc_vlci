-- Verify gis-sc_vlci:v_migrations/1605.vminut-agua-dato-anomalo on pg

BEGIN;

select 1/(1-count(*)) from sc_vlci.aguas_any_keyperformanceindicator where calculationperiod = '2023-03-26';

ROLLBACK;
