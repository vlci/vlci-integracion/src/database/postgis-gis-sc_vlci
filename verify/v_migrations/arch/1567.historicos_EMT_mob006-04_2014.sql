-- Verify gis-sc_vlci:v_migrations/1567.historicos_EMT_mob006-04_2014 on pg

BEGIN;

select case when count(*)=20019 then 1 else 1/(select 0) end as resultado  
from sc_vlci.emt_any_keyperformanceindicator
where entityid = 'mob006-04'
and calculationperiod < '2015-01-01';

ROLLBACK;
