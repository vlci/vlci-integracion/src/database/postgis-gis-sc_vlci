-- Verify gis-sc_vlci:v_migrations/765.Anyadir_contaminantes on pg

BEGIN;

select novalue, novalue_md, noxvalue, noxvalue_md, pm1value, pm1value_md, notype, notype_md, noxtype, noxtype_md, pm1type, pm1type_md
from sc_vlci.medioambiente_any_airqualityobserved; 

ROLLBACK;
