-- Verify gis-sc_vlci:v_migrations/1723-alter-inactive-offstreetparking on pg

BEGIN;

select inactive,inactive_md from trafico_any_offstreetparking limit 1;

ROLLBACK;
