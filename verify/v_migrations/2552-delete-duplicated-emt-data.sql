-- Verify gis-sc_vlci:v_migrations/2552-delete-duplicated-emt-data on pg

DO $$
DECLARE
    datos_23 integer;
    datos_24 integer;
    datos_esperados integer;
BEGIN
   datos_esperados := 80;
   datos_23 := (select count(*) from sc_vlci.emt_any_keyperformanceindicator where calculationperiod = '2024-02-23');
   ASSERT datos_23 = datos_esperados;
   datos_24 := (select count(*) from sc_vlci.emt_any_keyperformanceindicator where calculationperiod = '2024-02-24');
   ASSERT datos_24 = datos_esperados;
END $$;
