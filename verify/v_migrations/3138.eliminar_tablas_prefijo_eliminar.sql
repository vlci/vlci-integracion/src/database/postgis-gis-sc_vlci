-- Verify gis-sc_vlci:v_migrations/3138.eliminar_tablas_prefijo_eliminar on pg

BEGIN;

-- Verify de la tabla eliminar_medioambiente_any_airqualityobserved y su respectivo trigger.
SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_catalog.pg_tables
WHERE schemaname = 'sc_vlci'
AND tablename = 'eliminar_medioambiente_any_airqualityobserved';

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM information_schema.triggers
where trigger_schema = 'sc_vlci'
and trigger_name = 'insert_airqualityobserved_to_estatautomaticas';


-- Verify de la tabla eliminar_medioambiente_emt_any_keyperformanceindicator y su respectivo index y trigger.
SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_catalog.pg_tables
WHERE schemaname = 'sc_vlci'
AND tablename = 'eliminar_medioambiente_emt_any_keyperformanceindicator';

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_catalog.pg_indexes
WHERE schemaname = 'sc_vlci'
AND indexname = 'idx_emt_id_calculationperiod';

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM information_schema.triggers
where trigger_schema = 'sc_vlci'
and trigger_name = 'update_sanitat_inserttimestamp';


-- Verify de la tabla eliminar_sanitat_any_keyperformanceindicator y su respectivo index.
SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_catalog.pg_tables
WHERE schemaname = 'sc_vlci'
AND tablename = 'eliminar_sanitat_any_keyperformanceindicator';

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_catalog.pg_indexes
WHERE schemaname = 'sc_vlci'
AND indexname = 'idx_sanitat_id_calperiod';


-- Verify de la tabla eliminar_trafico_any_offstreetparking y su respectivo trigger.
SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM pg_catalog.pg_tables
WHERE schemaname = 'sc_vlci'
AND tablename = 'eliminar_trafico_any_offstreetparking';

SELECT 1/(case count(*) when 0 then 1 else 0 end)
FROM information_schema.triggers
where trigger_schema = 'sc_vlci'
and trigger_name = 'insert_offstreetparking_to_aparcamientos';


ROLLBACK;
