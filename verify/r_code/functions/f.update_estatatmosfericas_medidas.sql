-- Verify gis-sc_vlci:r_code/functions/f.update_estatatmosfericas_medidas on pg

BEGIN;

SELECT 1 / CASE 
	WHEN MD5(
			(SELECT 
				pg_get_functiondef(
					(SELECT oid FROM pg_proc WHERE proname = 'update_estatatmosfericas_medidas' LIMIT 1)
				)
			)
		) = '15077b277a6844afc21d891b70a1c46d' 
	THEN 1 
	ELSE 0
END
;

ROLLBACK;
