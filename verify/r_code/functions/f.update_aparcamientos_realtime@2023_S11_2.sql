-- Verify gis-sc_vlci:r_code/functions/f.update_aparcamientos_realtime on pg

BEGIN;

SELECT 1 / CASE 
	WHEN MD5(
			(SELECT 
				pg_get_functiondef(
					(SELECT oid FROM pg_proc WHERE proname = 'update_aparcamientos_realtime' LIMIT 1)
				)
			)
		) = 'dcab4059f4aad0ebbe6b39c993214605' 
	THEN 1 
	ELSE 0
END
;

ROLLBACK;
