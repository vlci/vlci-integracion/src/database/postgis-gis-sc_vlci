-- Verify gis-sc_vlci:r_code/functions/f.current_timestamp_before_update on pg

BEGIN;

SELECT
    1/count(*)
FROM 
    information_schema.routines
WHERE 
    routine_type = 'FUNCTION' and 
    routine_name = 'fn_current_timestamp_before_update'
AND
    routine_schema = 'sc_vlci';

ROLLBACK;
