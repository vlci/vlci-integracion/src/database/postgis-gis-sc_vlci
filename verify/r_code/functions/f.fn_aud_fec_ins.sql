-- Verify gis-sc_vlci:r_code/functions/f.fn_aud_fec_ins on pg

BEGIN;

    SELECT has_function_privilege('sc_vlci.fn_aud_fec_ins()', 'execute');

ROLLBACK;
