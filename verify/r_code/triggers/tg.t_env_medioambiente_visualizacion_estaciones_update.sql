-- Verify gis-sc_vlci:r_code/functions/tg.t_env_medioambiente_visualizacion_estaciones_update on pg

BEGIN;

SELECT  1/count(*)         
FROM information_schema.triggers  
where event_object_table = 't_env_medioambiente_visualizacion_estaciones'
and trigger_name = 't_env_medioambiente_visualizacion_estaciones_before_update';

ROLLBACK;
